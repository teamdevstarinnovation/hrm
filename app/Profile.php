<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function resource()
    {
    	return $this->belongsTo('App\Resource');
    }

    public function workExperiences()
    {
    	return $this->hasMany('App\WorkExperience');
    }

    public function languages()
    {
    	return $this->hasMany('App\ForeignLanguage');
    }

    public function task()
    {
    	return $this->hasMany('App\Task');
    }

    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    // Que quan
    public function homeTown()
    {
    	return $this->belongsTo('App\City', 'address');
    }

    // Tinh/ tp
    public function address_City()
    {
    	return $this->belongsTo('App\City', 'current_address_city');
    }

    // Quan huyen
    public function address_District()
    {
    	return $this->belongsTo('App\District', 'current_address_district');
    }

    // Phuong
    public function address_Ward()
    {
    	return $this->belongsTo('App\Ward', 'current_address_ward');
    }

}
