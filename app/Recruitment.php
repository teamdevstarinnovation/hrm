<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recruitment extends Model
{
    public function company()
    {
    	return $this->belongsTo('App\Company', 'company_id');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
}
