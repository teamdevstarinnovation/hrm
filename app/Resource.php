<?php

namespace App;

use App\Profile;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public function profiles()
    {
        return $this->hasMany('App\Profile', 'resource_id');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function address_City()
    {
    	return $this->belongsTo('App\City', 'address');
    }
}
