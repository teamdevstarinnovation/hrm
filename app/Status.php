<?php

namespace App;

use App\Profile;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status'; 
    
    public function profiles()
    {
        return $this->hasMany('App\Profile', 'status_id');
    }

}
