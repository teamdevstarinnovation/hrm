<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    public function resources()
    {
        return $this->morphedByMany('App\Resource', 'taggable');
    }

    public function companies()
    {
        return $this->morphedByMany('App\Company', 'taggable');
    }

    public function profiles()
    {
        return $this->morphedByMany('App\Profile', 'taggable');
    }

    public function recruitments()
    {
        return $this->morphedByMany('App\Recruitment', 'taggable');
    }

}
