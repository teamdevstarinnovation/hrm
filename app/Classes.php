<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'teacher_id');
    }

    public function students()
    {
        return $this->hasMany('App\ProfileClasses', 'class_id', 'id')->where('class_id',$this->id); 
    }
}
