<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\SystemLogs;
use App\User;

class AdminController extends Controller
{
    public function __construct()
    {

    }

    public function getDateFormat()
    {
        return 'Y-m-d H:i:s.u';
    }

    //Index method for Admin Controller
    public function index()
    {
        $system_logs = SystemLogs::all();
        return view('admin.home', compact('system_logs'));
    }

}
