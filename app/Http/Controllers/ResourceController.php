<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\Note;
use App\SystemLogs;
use App\Tag;
use App\City;

class ResourceController extends Controller
{
    public function listResource()
    {
        $resources = Resource::all();
        $log = SystemLogs::where('logType', 'Resource')->get();
        return view('admin.resource.resource_list', compact('resources','log'));
    }

    public function addResource()
    {   
        $cities = City::orderBy('name','ASC')->get();
        return view('admin.resource.resource_add', compact('cities'));
    }       

    public function doAddNewResource(Request $request)
    {
        $request->validate([
            'resource_name' => 'required',
            'resource_address' => 'required',
            'resource_phone' => 'required|regex:/[0-9]/'
        ],
        [
          'resource_name.required'=>'Tên nguồn không được để trống!',
          'resource_address.required'=>'Địa chỉ không được để trống!',
          'resource_phone.required'=> 'Số điện thoại không được để trống!',
          'resource_phone.regex'=>'Số điện thoại phải nhập từ 0..9!',
        ]);

        $rs = new Resource;

        $rs->name = $request->resource_name;
        $rs->address = $request->resource_address;
        $rs->phone = $request->resource_phone;

        $rs->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa thêm mới nguồn ".$request->resource_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Resource";
        $log->save();

        return redirect()->route('listResource');
    }

    public function editResource($id)
    {   
        $cities = City::orderBy('name','ASC')->get();

        $resource = Resource::where('id', $id)->first();
        $note = Note::where('destination_id', $id)->where('noteType', 'resource')
        ->join('users', 'notes.user_id', '=', 'users.id')
        ->get();
        $tag = $resource->tags;
        //dd($tag[1]->getOriginal('pivot_taggable_type'));
        if(!$resource){
            return redirect()->route('404');
        }
        return view('admin.resource.resource_edit',compact('resource','note','cities'));
    }

    public function doEditResource(Request $request)
    {
        $request->validate([
            'resource_name' => 'required',
            'resource_address' => 'required',
            'resource_phone' => 'required|regex:/[0-9]/'
        ],
        [
          'resource_name.required'=>'Tên nguồn không được để trống!',
          'resource_address.required'=>'Địa chỉ không được để trống!',
          'resource_phone.required'=> 'Số điện thoại không được để trống!',
          'resource_phone.regex'=>'Số điện thoại phải nhập từ 0..9!',
        ]);
        $rs_id = $request->input('resource_id');
        $rs = Resource::find($rs_id);
        $rs->name = $request->resource_name;
        $rs->address = $request->resource_address;
        $rs->phone = $request->resource_phone;

        $rs->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa chỉnh sửa nguồn ".$rs->name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Resource";
        $log->save();

        return redirect()->route('listResource');
    }

    public function doDeleteResource($id, $idU)
    {

        $rs = Resource::find($id);

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa xóa nguồn ".$rs->name;
        $log->content_jp = "";
        $log->user_id = $idU;
        $log->logType = "Resource";
        $log->save();

        Resource::destroy($id);
        return redirect()->route('listResource');
    }
}
