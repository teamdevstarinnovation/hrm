<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes;
use App\Note;
use App\User;
use App\Profile;
use App\ProfileClasses;
use App\SystemLogs;

class ClassController extends Controller
{
    public function listClass(){
        $classes = Classes::all();
        $log = SystemLogs::where('logType', 'Class')->get();
        return view('admin.class.dsclass', compact('classes','log'));
    }

    public function addClass(){
        $users = User::role(['thuky','truongphong'])->get();

        return view('admin.class.addclass', compact('users'));
    }

    public function doAddClass(Request $request){
        $request->validate([
            'class_name' => 'required',
            'teacher_id' => 'required'
        ],
        [
          'class_name.required'=>'Tên lớp không được để trống!',
          'teacher_id.required'=>'Giáo viên không được để trống!'
        ]);

        $c = new Classes;

        $c->name = $request->class_name;
        $c->teacher_id = $request->teacher_id;
        $c->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa thêm mới lớp học ".$request->class_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Class";
        $log->save();
        return redirect()->route('class.list');
    }

    public function editClass($id) {

        $users = User::role(['thuky','truongphong'])->get();
        $profiles = Profile::all();

        $classes = Classes::where('id', $id)->first();

        $note = Note::where('destination_id', $id)->where('noteType', 'resource')
        ->join('users', 'notes.user_id', '=', 'users.id')
        ->get();
        
        $profile_classes = ProfileClasses::where('class_id', $id)
        ->join('profiles', 'profile_classes.profile_id', '=', 'profiles.id')
        ->join('classes', 'profile_classes.class_id', '=', 'classes.id')
        ->get();
        
        $profileID_l = array();
        foreach ($profile_classes as $p) 
            {
                $profileID_l[] = $p->profile_id;
            }
         $profiles = Profile::whereNotIn('id',$profileID_l)->get();

        if(!$classes){
            return redirect()->route('404');
        }

        return view('admin.class.class_edit',compact('classes', 'note', 'users', 'profile_classes', 'profiles'));
    }

    public function doEditClass(Request $request) {
        $request->validate([
            'class_name' => 'required',
            'teacher_id' => 'required|regex:/[0-9]/'
        ],
        [
          'class_name.required'=>'Tên lớp không được để trống!',
          'teacher_id.required'=>'Giáo viên không được để trống!',
          'teacher_id.regex'=>'Yêu cầu nhập số!',
        ]);
        
        $c_id = $request->input('class_id');
        $c = Classes::find($c_id);

        $c->name = $request->class_name;
        $c->teacher_id = $request->teacher_id;
        $c->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa chỉnh sửa lớp học ".$request->class_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Class";
        $log->save();


        return redirect()->route('class.list');
    }

    public function doDeleteClass($id,$idU) {
        $c = Classes::find($id);

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa xóa lớp học ".$c->name;
        $log->content_jp = "";
        $log->user_id = $idU;
        $log->logType = "Class";
        $log->save();

        Classes::destroy($id);
        return redirect()->route('class.list');
    }

    public function doAddProfile(Request $request)
    {

        if (is_array($request->checked) || is_object($request->checked))
        {
            foreach ($request->checked as $key => $val) 
            {
                $pc = new ProfileClasses;
                if (in_array($val, $request->checked))
                {
                    $pc->class_id = $request->destination_id;
                    $pc->profile_id = $val;
                    $pc->save();
                }
            }
        }
         
        return redirect()->route('editClass',$request->destination_id);
    }

    public function doDeleteApplyClass($id,$idA)
    {
        

        ProfileClasses::where('profile_id',$id)->delete();
        return redirect()->route('editClass',$idA);
    }

}
