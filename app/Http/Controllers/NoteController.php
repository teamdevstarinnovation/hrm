<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;

class NoteController extends Controller
{
    public function addNoteCompany(Request $request) {

        $request->validate([
            'content' => 'required',
            'user_id' => 'required',
            'noteType' => 'required',
            'destination_id' => 'required'
        ],
        [
            'content.required' => 'Nội dung không được để trống!',
            'user_id.required' => 'Thiếu user id',
            'noteType.required' => 'Thiếu loại note',
            'destination_id.required' => 'Thiếu company id',

        ]);
        
        $note = new Note;

        $note->content = $request->content;
        $note->user_id = $request->user_id;
        $note->noteType = $request->noteType;
        $note->destination_id = $request->destination_id;

        $note->save();

        return redirect()->route('listComapny');

    }

    public function addNote(Request $request) {
        
        $noteType = $request->noteType;
        $direct = '';
    
        if ($noteType == 'company') {
            $direct = 'editCompany';
        } else if ($noteType == 'profile') {
            $direct = 'profile.detail';
        } else if ($noteType == 'recruitment') {
            $direct = 'recruiment.edit';
        } else if ($noteType == 'resource') {
            $direct = 'editResource';
        } else if($noteType == 'classes') {
            $direct = 'editClass';
        } 

        $request->validate([
            'content' => 'required',
            'user_id' => 'required',
            'noteType' => 'required',
        ],
        [
            'content.required' => 'Nội dung không được để trống!',
            'user_id.required' => 'Thiếu user id',
            'noteType.required' => 'Thiếu loại note',
        ]);
        
        $note = new Note;

        $note->content = $request->content;
        $note->user_id = $request->user_id;
        $note->noteType = $noteType;
        $note->destination_id = $request->destination_id;

        $note->save();

        return redirect()->route($direct,$request->destination_id);

    }

}
