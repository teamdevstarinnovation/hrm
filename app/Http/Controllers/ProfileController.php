<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Resource;
use App\Status;
use App\Note;
use App\SystemLogs;
use App\City;
use App\District;
use App\Ward;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
     // Show list profile
     public function profile()
     {
         $profiles = Profile::all();
         $log = SystemLogs::where('logType', 'Profile')->get();
         return view('admin.profile.dsungvien', compact('profiles','log'));
     }
     
     public function districts(Request $request) {
        $district = District::orderBy('name','ASC')->where('city_id','=',$request->cityid)->get();
        return response()->json($district);

     }

     public function wards(Request $request) {
        $ward = Ward::orderBy('name','ASC')->where('district_id','=',$request->districtid)->get();
        return response()->json($ward);
     }

     // Show form add profile
     public function getFormAddProfile()
     {
         $resources = Resource::all();
         $cities = City::orderBy('name','ASC')->get();
         $status = Status::all();   
         return view('admin.profile.addprofile_v2', compact('resources','status','cities'));
     }
 
     public function getFormEditProfile($id)
     {
         $profile = Profile::where('id', $id)->first();
         $cities = City::orderBy('name','ASC')->get();
         $districts = District::orderBy('name','ASC')->get();
         $wards = Ward::orderBy('name','ASC')->get();
         $resources = Resource::all();
         $status = Status::all();
         $father_birth = Carbon::createFromFormat('yy-m-d', $profile->father_year_of_birth)->format('Y');
         $mother_birth = Carbon::createFromFormat('yy-m-d', $profile->mother_year_of_birth)->format('Y');
        
         $degree_from = Carbon::createFromFormat('yy-m-d', $profile->degree_from)->format('Y-m');
         $degree_to = Carbon::createFromFormat('yy-m-d', $profile->degree_to)->format('Y-m');
         if(!$profile){
             return redirect()->route('404');
         }
 
         return view('admin.profile.editprofile', compact('profile','resources','status','father_birth','mother_birth','degree_from','degree_to','cities','districts','wards'));
     }

     public function getDetailProfile($id)
    {
        $profile = Profile::where('id', $id)->first();

        $note = Note::where('destination_id', $id)->where('noteType', 'profile')
        ->join('users', 'notes.user_id', '=', 'users.id')
        ->get();
        $resources = Resource::all();
        
        $father_birth = Carbon::createFromFormat('yy-m-d', $profile->father_year_of_birth)->format('Y');
        $mother_birth = Carbon::createFromFormat('yy-m-d', $profile->mother_year_of_birth)->format('Y');
       
        $degree_from = Carbon::createFromFormat('yy-m-d', $profile->degree_from)->format('Y-m');
        $degree_to = Carbon::createFromFormat('yy-m-d', $profile->degree_to)->format('Y-m');


        if(!$profile){
            return redirect()->route('404');
        }

        return view('admin.profile.detailProfile', compact('profile','note','father_birth','mother_birth','degree_from','degree_to'));
    }

    public function doDeleteProfile($id, $idU)
    {
        $profile = Profile::where('id', $id)->first();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa xóa hồ sơ của ". $profile->full_name;
        $log->content_jp = "";
        $log->user_id = $idU;
        $log->logType = "Profile";
        $log->save();


        Profile::destroy($id);

        

        return redirect()->route('profile.list');
    }

    public function doAddNewProfile(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'birthday' => 'required',
            'address' => 'required',
            'current_address_city' => 'required',
            'current_address_district' => 'required',
            'current_address_ward' => 'required',
            'gender' => 'required',
            'phone_number' => 'required|regex:/[0-9]/',
            'email' => 'required|unique:profiles,email|unique:users,email',
            'siblings_brothers' => 'regex:/[0-9]/',
            'siblings_younger_brothers' => 'regex:/[0-9]/',
            'siblings_sisters' => 'regex:/[0-9]/',
            'siblings_younger_sisters' => 'regex:/[0-9]/'
        ],
        [
          'full_name.required'=>'Tên nguồn không được để trống!',
          'birthday.required'=>'Ngày sinh không được để trống!',
          'address.required'=>'Quên quán không được để trống!',
          'current_address_city.required'=>'Địa chỉ hiện tại không được để trống!',
          'current_address_district.required'=>'Địa chỉ hiện tại không được để trống!',
          'current_address_ward.required'=>'Địa chỉ hiện tại không được để trống!',
          'gender.required'=>'Vui lòng chọn giới tính!',
          'email.required'=>'Địa chỉ email không được để trống!',
          'phone_number.required'=> 'Số điện thoại không được để trống!',
          'phone_number.regex'=>'Số điện thoại phải nhập từ 0..9!',
          'email.unique' => 'Địa chỉ email này đã tồn tại, vui lòng nhập địa chỉ email khác',
          'siblings_brothers.regex'=>'Số anh trai phải nhập số từ 0..9!', 
          'siblings_younger_brothers.regex'=>'Số em trai phải nhập số từ 0..9!', 
          'siblings_sisters.regex'=>'Số chị gái phải nhập số từ 0..9!', 
          'siblings_younger_sisters.regex'=>'Số em gái phải nhập số từ 0..9!', 
        ]);

        $p = new Profile();

        $p->full_name = $request->full_name;
        $p->birth = Carbon::parse($request->birthday);
        $p->address = $request->address;

        $p->current_address_city = $request->current_address_city;
        $p->current_address_district = $request->current_address_district;
        $p->current_address_ward = $request->current_address_ward;
        $p->gender = $request->gender;
        $p->email = $request->email;
        $p->phone_number = $request->phone_number;
        $p->resource_id = $request->resource;    
        $p->status_id = $request->status;    

        $p->father_fullname = $request->father_full_name;
        $p->father_year_of_birth = Carbon::parse($request->father_year_of_birth);
        $p->father_career = $request->father_career;
        $p->father_phone = $request->father_phone;

        $p->mother_fullname = $request->mother_full_name;
        
        $p->mother_year_of_birth = Carbon::parse($request->mother_year_of_birth);
        $p->mother_career = $request->mother_career;
        $p->mother_phone = $request->mother_phone;

        $p->siblings_brothers = $request->siblings_brothers === "" ? 0 : $request->siblings_brothers;
        $p->siblings_younger_brothers = $request->siblings_younger_brothers === "" ? 0 : $request->siblings_younger_brothers;
        $p->siblings_sisters = $request->siblings_sisters === "" ? 0 : $request->siblings_sisters;
        $p->siblings_younger_sisters = $request->siblings_younger_sisters === "" ? 0 : $request->siblings_younger_sisters;

        $p->degree_college_name = $request->degree_college_name;
        $p->degree_level = $request->degree_level;
        $p->degree_specialized = $request->degree_specialized;
        $p->degree_from =  Carbon::parse($request->degree_from);
        $p->degree_to =  Carbon::parse($request->degree_to);

        $p->special_skill = $request->special_skill;
        $p->other_certificate = $request->other_certificate;
        
        $p->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa mới thêm mới 1 hồ sơ cho ".$request->full_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Profile";
        $log->save();

        return redirect()->route('profile.list');
    }

    // do add new profile. method = POST
    public function doEditProfile(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'birthday' => 'required',
            'address' => 'required',
            'current_address_city' => 'required',
            'current_address_district' => 'required',
            'current_address_ward' => 'required',
            'gender' => 'required',
            'phone_number' => 'required|regex:/[0-9]/',
            'email' => 'required',
            'siblings_brothers' => 'regex:/[0-9]/',
            'siblings_younger_brothers' => 'regex:/[0-9]/',
            'siblings_sisters' => 'regex:/[0-9]/',  
            'siblings_younger_sisters' => 'regex:/[0-9]/'
        ],
        [
          'full_name.required'=>'Tên nguồn không được để trống!',
          'birthday.required'=>'Ngày sinh không được để trống!',
          'address.required'=>'Quên quán không được để trống!',
          'current_address_city.required'=>'Địa chỉ hiện tại không được để trống!',
          'current_address_district.required'=>'Địa chỉ hiện tại không được để trống!',
          'current_address_ward.required'=>'Địa chỉ hiện tại không được để trống!',
          'gender.required'=>'Vui lòng chọn giới tính!',
          'email.required'=>'Địa chỉ email không được để trống!',
          'phone_number.required'=> 'Số điện thoại không được để trống!',
          'phone_number.regex'=>'Số điện thoại phải nhập từ 0..9!', 
          'siblings_brothers.regex'=>'Số anh trai phải nhập số từ 0..9!', 
          'siblings_younger_brothers.regex'=>'Số em trai phải nhập số từ 0..9!', 
          'siblings_sisters.regex'=>'Số chị gái phải nhập số từ 0..9!', 
          'siblings_younger_sisters.regex'=>'Số em gái phải nhập số từ 0..9!',
        ]);
        $p_id = $request->input('profile_id');
        //dd($p_id );
        $p = Profile::find($p_id);
        
        $p->full_name = $request->full_name;
        
        $p->birth = Carbon::parse($request->birthday);
        $p->address = $request->address;

        $p->current_address_city = $request->current_address_city;
        $p->current_address_district = $request->current_address_district;
        $p->current_address_ward = $request->current_address_ward;   
        $p->gender = $request->gender;
        $p->email = $request->email;
        $p->phone_number = $request->phone_number;
        $p->resource_id = $request->resource;  
        $p->status_id = $request->status; 

        $p->father_fullname = $request->father_full_name;
        $p->father_year_of_birth = Carbon::parse($request->father_year_of_birth);
        $p->father_career = $request->father_career;
        $p->father_phone = $request->father_phone;

        $p->mother_fullname = $request->mother_full_name; 
        $p->mother_year_of_birth = Carbon::parse($request->mother_year_of_birth);
        $p->mother_career = $request->mother_career;
        $p->mother_phone = $request->mother_phone;

        $p->siblings_brothers = $request->siblings_brothers;
        $p->siblings_younger_brothers = $request->siblings_younger_brothers;
        $p->siblings_sisters = $request->siblings_sisters;
        $p->siblings_younger_sisters = $request->siblings_younger_sisters;

        $p->degree_college_name = $request->degree_college_name;
        $p->degree_level = $request->degree_level;
        $p->degree_specialized = $request->degree_specialized;
        $p->degree_from =  Carbon::parse($request->degree_from);
        $p->degree_to =  Carbon::parse($request->degree_to);

        $p->special_skill = $request->special_skill;
        $p->other_certificate = $request->other_certificate;
        //dd($p);
        $p->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa chỉnh sửa hồ sơ của ".$p->full_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Profile";
        $log->save();

        return redirect()->route('profile.list');
    }
}
