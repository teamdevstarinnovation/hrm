<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\User;
use App\Services\FacebookPoster;
use Illuminate\Support\Facades\Auth;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

class SocialAuthController extends Controller
{
    private $api;
    public function __construct(Facebook $fb)
    {   
        $this->api = $fb;
    }

   public function fb_redirect()
  {
   //   return Socialite::driver('facebook')->scopes([
   //    "publish_actions, manage_pages", "publish_pages"])->redirect();
      return Socialite::driver('facebook')->redirect();
  }

 public function fb_callback()
 {
    try {

        $user = Socialite::driver('facebook')->user();

        $currentUser = Auth::user();
        $currentUser->token = $user->token;
        $currentUser->save();
         
        // $create['name'] = $user->getName();
        // $create['email'] = $user->getEmail();
        // $create['facebook_id'] = $user->getId();

        // $userModel = new User;
        // $createdUser = $userModel->addNew($create);
        // Auth::loginUsingId($createdUser->id);

        $token = $currentUser->token;
        $this->api->setDefaultAccessToken($token);

        // $params = "first_name,last_name,age_range,gender";
        // $user = $this->api->get('/me?fields='.$params)->getGraphUser();
        $res = $this->publishToProfile($token);
        var_dump($res);
        die();
        //return redirect()->route('fb.user');

        // var_dump($user);
        // die();

    } catch (Exception $e) {
        return redirect('redirect');
    }
  }

  public function publishToProfile($token){
        try {

            $response = $this->api->post(
                '/me/feed',
                array (
                  'message' => 'Awesome!'
                ),
                $token
            );

            // $response = $this->api->post('/me/feed', [
            //     'message' => 'Test post msg to my feed'
            // ])->getGraphNode()->asArray();

            if($response['id']){
                // post created
                return $response;
            }
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
        return null;
    }


    public function google_redirect(){
        return Socialite::driver('google')->redirect();
    }

    public function google_callback() {
        try {

            $user = Socialite::driver('google')->user();

            $finduser = User::where('google_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);

                return redirect('/home');

            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id
                ]);

                Auth::login($newUser);

                return redirect()->back();
            }

        } catch (Exception $e) {
            return redirect()->route('google.redirect');
        }
        
    }
}
