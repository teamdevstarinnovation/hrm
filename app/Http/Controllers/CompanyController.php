<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Note;
use App\SystemLogs;

class CompanyController extends Controller
{
    // Company
    public function listCompany()
    {
        $company = Company::all();
        $log = SystemLogs::where('logType', 'Company')->get();
        return view('admin.company.dscompany', compact('company','log'));
    }

    public function addCompany()
    {
        return view('admin.company.addcompany');
    }

    public function doAddNewCompany(Request $request)
    {
        $request->validate([
            'company_name' => 'required',
            'industry_type' => 'required',
            'field_of_activity' => 'required',
            'director_name' => 'required'
        ],
        [
          'company_name.required'=>'Tên công ty không được để trống!',
          'industry_type.required'=>'Loại công nghiệp không được để trống!',
          'field_of_activity.required'=> 'Lĩnh vực hoạt động không được để trống!',
          'director_name.required'=>'Người điều hành không được để trống!',
        ]);

        $company = new Company;

        $company->name = $request->company_name;
        $company->industry_type = $request->industry_type;
        $company->field_of_activity = $request->field_of_activity;
        $company->director_name = $request->director_name;

        $company->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa thêm mới công ty ".$request->company_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Company";
        $log->save();

        return redirect()->route('company.list');
    }

    public function editCompany($id)
    {   
        
        $company = Company::where('id', $id)->first();
        $note = Note::where('destination_id', $id)->where('noteType', 'company')
        ->join('users', 'notes.user_id', '=', 'users.id')
        ->get();
        
        if(!$company){
            return redirect()->route('404');
        }

        return view('admin.company.company_edit',compact('company','note'));
    }


    public function doEditCompany(Request $request)
    {
        $request->validate([
            'company_name' => 'required',
            'industry_type' => 'required',
            'field_of_activity' => 'required',
            'director_name' => 'required'
        ],
        [
          'company_name.required'=>'Tên công ty không được để trống!',
          'industry_type.required'=>'Loại công nghiệp không được để trống!',
          'field_of_activity.required'=> 'Lĩnh vực hoạt động không được để trống!',
          'director_name.required'=>'Người điều hành không được để trống!',
        ]);
        $company_id = $request->input('company_id');
        $company = Company::find($company_id);
        $company->name = $request->company_name;
        $company->industry_type = $request->industry_type;
        $company->field_of_activity = $request->field_of_activity;
        $company->director_name = $request->director_name;

        $company->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa chỉnh sửa công ty ".$company->name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Company";
        $log->save();

        return redirect()->route('company.list');
    }

    public function doDeleteCompany($id, $idU)
    {
        $company = Company::find($id);

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa xóa công ty ".$company->name;
        $log->content_jp = "";
        $log->user_id = $idU;
        $log->logType = "Company";
        $log->save();

        Company::destroy($id);
        return redirect()->route('company.list');
    }
}
