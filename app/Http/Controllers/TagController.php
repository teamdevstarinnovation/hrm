<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Response;
use App\Resource;
use App\Company;
use App\Recruitment;


class TagController extends Controller
{
    public function doAddTag(Request $request) {
       
        $taggable_type = $request->taggable_type;
        $taggable_id = $request->taggable_id;

        if($taggable_type === 'App\Resource'){
            $resource = Resource::find($taggable_id);
            if($resource){
                $tag = new Tag;
                $tag->name = $request->name;
                $tag->name_jp = $request->name_jp;
    
                $resource->tags()->save($tag);
                
                return Response::json("{ success : 'true' }");
            }
        }else if($taggable_type === 'App\Company'){
            $company = Company::find($taggable_id);
            if($company){
                $tag = new Tag;
                $tag->name = $request->name;
                $tag->name_jp = $request->name_jp;
    
                $company->tags()->save($tag);
                
                return Response::json("{ success : 'true' }");
              }
        }else if($taggable_type === 'App\Recruitment'){
            $recruitment = Recruitment::find($taggable_id);
            if($recruitment){
                $tag = new Tag;
                $tag->name = $request->name;
                $tag->name_jp = $request->name_jp;
    
                $recruitment->tags()->save($tag);
                
                return Response::json("{ success : 'true' }");
              }
        }
        return Response::json("{ success : 'false' }");
    }

    public function doDeletetag(Request $request) {
       
        $taggable_type = $request->taggable_type;
        $taggable_id = $request->taggable_id;

        if($taggable_type === 'App\Resource'){
            
            $resource = Resource::find($taggable_id);
            $tag = Tag::where('name', $request->name)->first();
            $resource->tags()->detach($tag->id);            
            return Response::json("{ success : 'true' }");
            
        }else if($taggable_type === 'App\Company'){
            
            $company = Company::find($taggable_id);
            $tag = Tag::where('name', $request->name)->first();
            $company->tags()->detach($tag->id);            
            return Response::json("{ success : 'true' }");
            
        }else if($taggable_type === 'App\Recruitment'){
            
            $recruitment = Recruitment::find($taggable_id);
            $tag = Tag::where('name', $request->name)->first();
            $recruitment->tags()->detach($tag->id);            
            return Response::json("{ success : 'true' }");
            
        }
        return Response::json("{ success : 'false' }");
    }

}
