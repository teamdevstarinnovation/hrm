<?php

namespace App\Http\Controllers;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\SystemLogs;

class UserController extends Controller
{
    
    public function listUser() {

        $users = User::all();
        $log = SystemLogs::where('logType', 'User')->get();
        return view('admin.user.user_list', compact('users','log'));

    }

    public function addUser() {
        $roles = Role::all();
        return view('admin.user.user_add', compact('roles'));
    }

    public function doAddUser(Request $request) 
    {
        
        $request->validate([
            'user_name' => 'required',
            'user_email' => 'required',
            'user_password' => 'required'
        ],
        [
            'user_name.required' => 'Tên người dùng không được để trống !',
            'user_email.required' => 'Email không được để trống !',
            'user_password.required' => 'Password không được để trống !'
        ]);
        
        $u = new User;
        $u->name = $request->user_name; 
        $u->email = $request->user_email; 
        $u->password = bcrypt($request->user_password);
        $u->save();

        $role = $request->role;
        $u->assignRole($role);

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa thêm mới User ".$request->user_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "User";
        $log->save();

        return redirect()->route('listUser');

    }

    public function editUser($id) 
    {
        $user = User::where('id', $id)->first();
        if(!$user){
            return redirect()->route('404');
        }

        $roles = Role::all();
        return view('admin.user.user_edit',compact('user', 'roles'));
    }

    public function doEditUser(Request $request)
    {
        $request->validate([
            'user_name' => 'required',
        ],
        [
            'user_name.required' => 'Tên người dùng không được để trống !',
        ]);
        
        $u_id = $request->input('user_id');
        $u = Recruitment::find($u_id);
        $u->name = $request->user_name; 

        if ($strTemp !== ''){
            $u->password = bcrypt($request->user_password);
        }
        
        $u->save();
        $role = $request->role;
        $u->assignRole($role);

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa chỉnh sửa User ".$u->name;
        $log->content_jp = "";
        $log->user_id = $request->user_id2;
        $log->logType = "User";
        $log->save();

        return redirect()->route('listUser');
    }

    public function deDeleteUser($id, $idU) 
    {
        $u = User::find($id);

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa xóa nguồn ".$u->name;
        $log->content_jp = "";
        $log->user_id = $idU;
        $log->logType = "Resource";
        $log->save();

        User::destroy($id);
        return redirect()->route('listUser');
    }

}
