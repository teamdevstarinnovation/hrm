<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
     // Task 
     public function task()
     {
         $tasks = Task::all();
         return view('admin.task.task_list', compact('tasks'));
     }
 
     public function getFormAddNewTask()
     {
         return view('admin.task.task_add');
     }
 
     public function doAddNewTask(Request $request)
     {
         $request->validate([
             'nameTask' => 'required',
             'contentTask' => 'required',
             'deadline' => 'required'
         ],
         [
           'nameTask.required'=>'Tên nhiệm vụ không được để trống!',
           'contentTask.required'=>'Nội dung nhiệm vụ không được để trống!',
           'deadline.required'=> 'Deadline không được để trống!',
         ]);
 
         $rs = new Resource;
 
         $rs->nameTask = $request->nameTask;
         $rs->contentTask = $request->contentTask;
         $rs->deadline = $request->deadline;
 
         $rs->save();
 
         return redirect()->route('task.list');
     }
 
 
     public function deleteTask($id)
     {
         Task::destroy($id);
         return redirect()->route('task.list');
     }
}
