<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recruitment;
use App\Company;
use App\Note;
use App\Apply;
use App\Profile;
use App\SystemLogs;
use Illuminate\Support\Carbon;

class RecruitmentController extends Controller
{
    // Quản lý đơn hàng --> Danh sách đơn hàng
    public function getRecruitmentList()
    {
        $recruitments = Recruitment::all();
        $log = SystemLogs::where('logType', 'Recruitment')->get();
        return view('admin.recruitment.recruitment_list', compact('recruitments','log'));
    }

    // Form thêm mới đơn hàng
    public function getFormAddRecruitment()
    {
        $companys = Company::all();
        return view('admin.recruitment.add_recruitment',compact('companys'));
    }

    // do add new recruiment
    public function doAddNewRecruitment(Request $request) 
    {
        $request->validate([
            'recruiment_name' => 'required',
            'recruiment_received_date' => 'required',
            'recruiment_interview_date' => 'required',
            'recruiment_visa' => 'required',
            'recruiment_position' => 'required',
            'recruiment_required_number' => 'required|regex:/^\d+$/',
            'recruiment_number_interview' => 'required|regex:/^\d+$/',
            'recruiment_workplace' => 'required',
            'recruiment_salary' => 'required|regex:/^\d+$/',
            'recruiment_description' => 'required'
        ],
        [
            'recruiment_name.required' => 'Tên đơn hàng không được để trống!',
            'recruiment_received_date.required' => 'Ngày nhận đơn hàng không được để trống!',
            'recruiment_interview_date.required' => 'Ngày phỏng vấn không được để trống!',
            'recruiment_visa.required' => 'Loại visa không được để trống!',
            'recruiment_position.required' => 'Vị trí tuyển dụng không được để trống!',
            'recruiment_required_number.required' => 'Sô lượng cần không được để trống!',
            'recruiment_required_number.regex' => 'Chỉ cho phép nhập số!',
            'recruiment_number_interview.required' => 'Số lượng tham gia phỏng vấn không được để trống!',
            'recruiment_number_interview.regex' => 'Chỉ cho phép nhập số!',
            'recruiment_workplace.required' => 'Nơi làm việc không được để trống',
            'recruiment_salary.required' => 'Mức lương không được để trống',
            'recruiment_salary.regex' => 'Chỉ cho phép nhập số!',
            'recruiment_description.required' => 'Mô tả công việc không được để trống'
        ]);

        $rc = new Recruitment;
        $rc->company_id = $request->company;
        $rc->name = $request->recruiment_name;
        $rc->received_date = Carbon::parse($request->recruiment_received_date);
        $rc->interview_date = Carbon::parse($request->recruiment_interview_date);
        $rc->visa_type = $request->recruiment_visa;
        $rc->position = $request->recruiment_position;
        $rc->require_numbers = $request->recruiment_required_number;
        $rc->numbers_of_interview = $request->recruiment_number_interview;
        $rc->work_place = $request->recruiment_workplace;
        $rc->salary = $request->recruiment_salary;
        $rc->description = $request->recruiment_description;

        $rc->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa thêm mới đơn hàng ".$request->recruiment_name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Recruitment";
        $log->save();


        return redirect()->route('recruitment.list');

    }

    //form recruiment
    public function getFormEditRecruiment($id)
    {
        $recruiment = Recruitment::where('id', $id)->first();
        $companies = Company::all();
        

        $note = Note::where('destination_id', $id)->where('noteType', 'recruitment')
        ->join('users', 'notes.user_id', '=', 'users.id')
        ->get();

        $profiles_apply = Apply::where('recruitment_id', $id)
        ->join('profiles', 'applies.profile_id', '=', 'profiles.id')
        ->join('users', 'applies.created_user_id', '=', 'users.id')
        ->get();

        $profileID_l = array();
        foreach ($profiles_apply as $p) 
            {
                $profileID_l[] = $p->profile_id;
            }
         $profiles = Profile::whereNotIn('id',$profileID_l)->get();

        if(!$recruiment){
            return redirect()->route('404');
        }
        return view('admin.recruitment.recruitment_edit',compact('recruiment','companies','note', 'profiles_apply','profiles'));
    }

    //do edit recruiment
    public function doEditRecruiment(Request $request)
    {
        $request->validate([
            'recruiment_name' => 'required',
            'recruiment_received_date' => 'required',
            'recruiment_interview_date' => 'required',
            'recruiment_visa' => 'required',
            'recruiment_position' => 'required',
            'recruiment_required_number' => 'required|regex:/[0-9]/',
            'recruiment_number_interview' => 'required',
            'recruiment_workplace' => 'required',
            'recruiment_salary' => 'required',
            'recruiment_description' => 'required'
        ],
        [
            'recruiment_name.required' => 'Tên đơn hàng không được để trống!',
            'recruiment_received_date.required' => 'Ngày nhận đơn hàng không được để trống!',
            'recruiment_interview_date.required' => 'Ngày phỏng vấn không được để trống!',
            'recruiment_visa.required' => 'Loại visa không được để trống!',
            'recruiment_position.required' => 'Vị trí tuyển dụng không được để trống!',
            'recruiment_required_number.required' => 'Sô lượng cần không được để trống!',
            'recruiment_required_number.regex' => 'Chỉ được phép nhập số!',
            'recruiment_number_interview.required' => 'Số lượng tham gia phỏng vấn không được để trống!',
            'recruiment_workplace.required' => 'Nơi làm việc không được để trống',
            'recruiment_salary.required' => 'Mức lương không được để trống',
            'recruiment_description.required' => 'Mô tả công việc không được để trống'
        ]);

        $rc_id = $request->input('recruitment_id');
        $rc = Recruitment::find($rc_id);
        $rc->company_id = $request->company;
        $rc->name = $request->recruiment_name;
        $rc->received_date = $request->recruiment_received_date;
        $rc->interview_date = $request->recruiment_interview_date;
        $rc->visa_type = $request->recruiment_visa;
        $rc->position = $request->recruiment_position;
        $rc->require_numbers = $request->recruiment_required_number;
        $rc->numbers_of_interview = $request->recruiment_number_interview;
        $rc->work_place = $request->recruiment_workplace;
        $rc->salary = $request->recruiment_salary;
        $rc->description = $request->recruiment_description;

        $rc->save();

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa chỉnh sửa đơn hàng ".$rc->name;
        $log->content_jp = "";
        $log->user_id = $request->user_id;
        $log->logType = "Recruitment";
        $log->save();

        return redirect()->route('recruitment.list');
    }
    

    public function doDeleteRecruitment($id, $idU)
    {
        $rc = Recruitment::find($id);

        $log = new SystemLogs();
        $log->content = "Quản trị viên vừa xóa đơn hàng ".$rc->name;
        $log->content_jp = "";
        $log->user_id = $idU;
        $log->logType = "Recruitment";
        $log->save();

        Recruitment::destroy($id);
        return redirect()->route('recruitment.list');
    }

    public function doAddProfile(Request $request)
    {

        if (is_array($request->checked) || is_object($request->checked))
        {
            foreach ($request->checked as $key => $val) 
            {
                $apply = new Apply;
                if (in_array($val, $request->checked))
                {
                    $apply->created_user_id = $request->user_id;
                    $apply->recruitment_id = $request->destination_id;
                    $apply->profile_id = $val;
                    $apply->save();
                }
            }
        }
         
        return redirect()->route('recruiment.edit',$request->destination_id);
    }

    public function doDeleteApply($id,$idA)
    {
        Apply::where('profile_id',$id)->delete();;
        return redirect()->route('recruiment.edit',$idA);
    }

}
