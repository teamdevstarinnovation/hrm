<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function recruitments()
    {
    	return $this->hasMany('App\Recruitment');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
}
