<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileClasses extends Model
{
    protected $table = 'profile_classes';

    public function classes()
    {
        return $this->belongsTo('App\Classes', 'class_id');
    }
}
