@extends('layouts.admin')
@section('customcss')
    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
@endsection
@section('content')

<h3 class="mt-4">Thêm mới đơn hàng</h3>

<div class="row">
    <div class="col-lg-6">

      @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
      @endif

    <form role="form" method="POST" action="{{ route('recruitment.store') }}">
      {{ csrf_field() }}
      <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
      <div class="form-group">
          <label>Công Ty:</label>
          <select id="company" name="company" placeholder="Chọn 1 Công Ty ...">
          @foreach ($companys as $item)
            <option value="{{ $item->id }}"> {{ $item->name }} </option>
          @endforeach     
        </select>
     </div>
        <div class="form-group">
          <label>Tên đơn hàng:</label>
          <input type="text" class="form-control" name="recruiment_name"  value="{{ old('recruiment_name') }}" autocomplete="off"/>
        </div>

        <div class="form-group">
          <label>Ngày nhận đơn:</label>
          <input type="text" class="form-control date" data-date-format="dd-mm-yyyy" name="recruiment_received_date" value="{{ old('recruiment_received_date') }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Ngày phỏng vấn:</label>
            <input type="text" class="form-control date" data-date-format="dd-mm-yyyy" name="recruiment_interview_date" value="{{ old('recruiment_interview_date') }}" autocomplete="off"/>
        </div>
        
        <div class="form-group">
            <label>Loại Visa:</label>
            <input type="text" class="form-control" name="recruiment_visa" value="{{ old('recruiment_visa') }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Vị trí tuyển dụng:</label>
            <input type="text" class="form-control" name="recruiment_position" value="{{ old('recruiment_position') }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Số lượng cần:</label>
            <input type="text" class="form-control" name="recruiment_required_number" value="{{ old('recruiment_required_number') }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Số lượng tham gia phỏng vấn:</label>
            <input type="text" class="form-control" name="recruiment_number_interview" value="{{ old('recruiment_number_interview') }}" autocomplete="off"autocomplete="off"/>
        </div>
        
        <div class="form-group">
            <label>Nơi làm việc:</label>
            <input type="text" class="form-control" name="recruiment_workplace" value="{{ old('recruiment_workplace') }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Lương bổng:</label>
            <input type="text" class="form-control" name="recruiment_salary" value="{{ old('recruiment_salary') }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Mô tả công việc:</label>
            <textarea class="form-control" id="recruiment_description" name="recruiment_description" rows="4" cols="50" autocomplete="off">{{ old('recruiment_description') }}</textarea>
        </div>

        <button type="submit" class="btn btn-success" style="margin-top: 10px">Thên mới đơn hàng</button>

      </form>

    </div>
  </div>
@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">
      jQuery().ready(function () {
        // init select date form
       
        $('.date').datepicker({ 
          weekStart: 1,
          daysOfWeekHighlighted: "6,0",
          autoclose: true,
          todayHighlight: true,
          locale: 'vi',
          
          });
        //$('.date').datepicker();

      });
      
      $('select').selectize({
          sortField: 'text'
      });
      
      tinymce.init({
        selector: '#recruiment_description',
        menubar: false,
        height: 300,
        toolbar_mode: 'wrap',
        toolbar_location: 'bottom',
        plugins: "link image code",
          toolbar: 'undo redo | styleselect | forecolor | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
        images_upload_handler: function (blobInfo, success, failure) {
          var xhr, formData;
          xhr = new XMLHttpRequest();
          xhr.withCredentials = false;
          xhr.responseType = 'json';
          xhr.open('POST', uploadImg2Url);
          xhr.onload = function() {
            var imageName;

            if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
            }
            imageName = xhr.response;
            console.log("Image name = "+ imageName);
            //var url = "${imgUrl}/images/".concat(imageName);
            success(imageName);
          };
          
          var fileName = "";
          if( typeof(blobInfo.blob().name) !== undefined )
              fileName = blobInfo.blob().name;
          else
              fileName = blobInfo.filename();
          
          formData = new FormData();
          formData.append('file', blobInfo.blob(), fileName);
          xhr.send(formData);
          },
          
          
          force_br_newlines : true,
          force_p_newlines : false,
          forced_root_block : '',
          //image_prepend_url: jobSearchRoot + "/images/",
          relative_urls : false,
          //document_base_url: jobSearchRoot + "/images/",
          valid_children : '+body[style]'
      });

    </script>
@endsection