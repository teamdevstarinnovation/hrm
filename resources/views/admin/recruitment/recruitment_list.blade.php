@extends('layouts.admin')
<style>
.action-button {
    width: 100px;
    background: skyblue;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px;
}
</style>
@section('content')

<h3 class="mt-4">Danh Sách Đơn Hàng - Cập nhật NewFeed</h3>
<div class="card mb-4 bg-warning">
    <div class="card-body">
        <ul> @foreach ($log as $l)
            <li>{{ $l->content }}</li>
            @endforeach  
        </ul>
    </div>
</div>
<div class="card mb-4">
    <div class="card-body">
        <input type="button" style="margin-bottom: 15px;" onclick="location.href='{{ route('recruitment.create') }}'" name="reigster" class="action-button" value="Thêm mới" />
        <span style="float:right" >SLYC =  Số Lượng Yêu Cầu / SLPV = Số Lượng Phỏng Vấn</span>
        <div class="table-responsive">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">                
                <div class="row"><div class="col-sm-12">
                <table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 120px">Tên Đơn Hàng</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 80px" >Ngày Nhận Đơn</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 80px" >Ngày Phỏng Vấn</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 20px" >Loại Visa</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: " >Vị trí tuyển dụng</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 20px" >SLYC</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 20px" >SLPV</th>
                            @if(auth()->user()->can('recruitments.delete'))
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width:" ></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        
                         @foreach ($recruitments as $r)
                            <tr role="row" class="odd">
                            <input type="hidden" name="user_id" id="user_id"  value="{{ auth()->user()->id }}"/>
                                <td class="sorting_1"><a href="{{ route('recruiment.edit', $r->id) }}"> {{ $r->name }} </a></td>
                                <td>{{ $r->received_date }}</td>
                                <td>{{ $r->interview_date }}</td>
                                <td>{{ $r->visa_type }}</td>
                                <td>{{ $r->position }}</td>
                                <td>{{ $r->require_numbers }}</td>
                                <td>{{ $r->numbers_of_interview }}</td>
                                @if(auth()->user()->can('recruitments.delete'))
                                    <td><a href="#" onclick="return openPopup({{ $r->id }});">Xóa</a></td>
                                @endif
                            </tr>
                         @endforeach

                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function openPopup(id){
    $.confirm({
        title: 'Xác nhận',
        content: 'Bạn có muôn xóa?',
        buttons: {
            confirm: function () {
                window.location.href = 'delete-recruitment/'+id+"-"+$("#user_id").val();
            },
            cancel: function () {
                //$.alert('Canceled!');
            },           
        }
    });
}
$( document ).ready(function() {
  $('#dataTable').DataTable({
    "pagingType": "full_numbers",
    "language": {
      "search": "Tìm kiếm: ",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "paginate" : {
        "first":    'Đầu',
        "previous": 'Trước',
        "next":     'Tiếp',
        "last":     'Cuối'
      }
    },
    "scrollY":        "300px",
    "scrollX":        true,
    "scrollCollapse": true,
    "paging":         true,
    "columnDefs": [
            { width: '16%', targets: 0 },
            { width: '16%', targets: 1 },
            { width: '18%', targets: 2 },
            { width: '16%', targets: 3 },
            { width: '18%', targets: 4 },
            { width: '5%', targets: 5 },
            { width: '5%', targets: 6 },
            { width: '5%', targets: 7 },
        ],
        "fixedColumns": true,
    "info" : false
  });
});
</script>

@endsection

@section('customjs')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
@endsection