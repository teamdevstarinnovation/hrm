@extends('layouts.admin')
@section('customcss')
    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <style>
    .add-note {
      background-color: #314963;
      height: 100px;
      width: 100px;
      position: fixed;
      bottom: 20%;
      right: 10%;
      border-radius: 50%;
      font-size: 70px;
      z-index: 999999;
    }

    .border-note {
      border-radius: 25px;
      border: 2px solid rgb(150, 150, 150);
      padding: 20px;
    }
  </style>
@endsection
@section('content')

<h3 class="mt-4">Chỉnh sửa đơn hàng</h3>

<div class="heading-block">
  <!--- Tags block -->
  <div class="tag-block"> 
  <ul id="myTags">
    <!-- Existing list items will be pre-added to the tags -->
    @foreach ($recruiment->tags as $item)
            <li>{{ $item->name }}</li>          
           
    @endforeach
  </ul>
  </div>
  <!-- End tags block-->

  <!-- Social block -->
  <div style="margin-left: 5px;">
    <a href="{{ url('/redirect') }}">
      <button class="btn btn-primary" disabled>Share to facebook</button>
    </a>
  </div>
  <!-- -->
</div>


<div class="row">
    <div class="col-lg-6">

      @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
      @endif

    <form role="form" method="POST" action="{{ route('recruitment.doEdit') }}">
      {{ csrf_field() }}
      <div class="form-group">
      <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
          <label>Công Ty:</label>
          <select id="company" name="company" placeholder="Chọn 1 Công Ty ...">
          @foreach ($companies as $item)
            <option value="{{ $item->id }}" {{ ( $item->id == $recruiment->company_id) ? 'selected' : '' }}> {{ $item->name }} </option>
          @endforeach 
        </select>
     </div>
      
        <div class="form-group">
          <input type="hidden" name="recruitment_id" value="{{ $recruiment->id }}" />
          <label>Tên đơn hàng:</label>
          <input type="text" class="form-control" name="recruiment_name"  value="{{ $recruiment->name }}" autocomplete="off"/>
        </div>

        <div class="form-group">
          <label>Ngày nhận đơn:</label>
          <input type="text" class="form-control date" name="recruiment_received_date" value="{{ $recruiment->received_date }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Ngày phỏng vấn:</label>
            <input type="text" class="form-control date" name="recruiment_interview_date" value="{{ $recruiment->interview_date }}" autocomplete="off"/>
        </div>
        
        <div class="form-group">
            <label>Loại Visa:</label>
            <input type="text" class="form-control" name="recruiment_visa" value="{{ $recruiment->visa_type }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Vị trí tuyển dụng:</label>
            <input type="text" class="form-control" name="recruiment_position" value="{{ $recruiment->position }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Số lượng cần:</label>
            <input type="text" class="form-control" name="recruiment_required_number" value="{{ $recruiment->require_numbers }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Số lượng tham gia phỏng vấn:</label>
            <input type="text" class="form-control" name="recruiment_number_interview" value="{{ $recruiment->numbers_of_interview }}" autocomplete="off"autocomplete="off"/>
        </div>
        
        <div class="form-group">
            <label>Nơi làm việc:</label>
            <input type="text" class="form-control" name="recruiment_workplace" value="{{ $recruiment->work_place }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Lương bổng:</label>
            <input type="text" class="form-control" name="recruiment_salary" value="{{ $recruiment->salary }}" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label>Mô tả công việc:</label>
            <textarea class="form-control" id="recruiment_description" name="recruiment_description" rows="4" cols="50" autocomplete="off">{{ $recruiment->description }}</textarea>
        </div>

        <button type="submit" class="btn btn-success" style="margin-top: 10px">Cập nhật đơn hàng</button>  

      </form>
    </div>

    <div class="col-lg-6">  
      <div class="row">
        <h5 style="text-align: center;">Tổng số ứng viên tham gia đơn hàng ({{ count($profiles_apply) }}) </h5>
        <hr />
        <ul style="list-style-type: none;">
          @foreach ($profiles_apply as $item)
              <li><a href="{{ route('profile.detail', $item->profile_id) }}"> {{ $item->full_name }} </a> được thêm bởi {{ $item->name }} | {{ $item->created_at }} 
              <a href="#" onclick="return theFunction({{$item->recruitment_id }},{{ $item->profile_id}});"><i  class="fa fa-times" aria-hidden="true"></i></a></li>
          @endforeach
          
        </ul>

        <button type="button" id="addProfile" class="btn btn-success" style="">Thêm ứng viên</button>
      </div>
      
      <div class="row" style="margin-top: 50px;">
        <h5 style="text-align: center;">Thống kê đơn hàng </h5>
        <hr />
        <ul style="list-style-type: none;">
         <li>Đã có 3 ứng viên của nguồn Nguyễn Văn A tham gia</li>
         <li>Công ty này đã có 5 đơn hàng cần tuyển người</li>
        </ul>
      </div>

    </div>

    <div class="col-lg-6">
        <button type="button" class="btn btn-info btn-lg add-note" data-toggle="modal" data-target="#myModal"><i style="margin-bottom: 12px;" class="fa fa-plus" aria-hidden="true"></i></button>
    </div>
  </div>

  <div class="row form-group border-note">
      <p><h3 style="width: 100%">Danh sách ghi chú</h3></p>
      <br>
      @if(isset($note) && is_null($note))
        <p>Không có note nào</p>
      @else
        @foreach ($note as $item)
            <p style="width: 100%">{{ $item->created_at }} - {{ $item->name }} - {{ $item->content }}</p>
        @endforeach
      @endif
  </div>
  
  <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <form role="form" method="POST" action="{{ route('addNote') }}">
          {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Ghi chú</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <textarea id="content" name = "content" style="width: 100%; resize: none;" rows="5" placeholder="Điền ghi chú ở đây"></textarea>
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <input type="hidden" name="noteType"  value="recruitment" />
                <input type="hidden" name="destination_id"  value="{{ $recruiment->id }}"/>
              </div>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-default">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>

       <!-- Modal -->
       <div class="modal fade" id="ModalAddprofile" role="dialog">
        <div class="modal-dialog">
          <form role="form"  method="POST" action="{{ route('recruitment.doAddProfile') }}">
          {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Danh sách ứng viên</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                      @foreach($profiles as $p)
                            <tr>
                              <td>
                              <input type="checkbox" name="checked[]" id="myCheck{{ $p->id }}" value="{{ $p->id }}">
                              </td>
                              <td><label for="myCheck{{ $p->id }}">{{$p->full_name }}</label> </td>
                            </tr>
                            <br>
                            @endforeach
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <input type="hidden" name="noteType"  value="recruitment" />
                <input type="hidden" name="destination_id"  value="{{ $recruiment->id }}" />
              </div>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-default">Thêm</button>
              </div>
            </div>
          </form>
        </div>
      </div>

@endsection

@section('customjs')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('tinymce/tinymce.min.js') }}"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

    <script src="{{ asset('admincp/js/tag-it.js') }}" ></script>

    <script>
    $.noConflict();
    jQuery(document).ready(function(){
      var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
     
      jQuery("#myTags").tagit({
          availableTags: sampleTags,  
          removeConfirmation: true,
            allowDuplicates: false,
            allowSpaces: true,
          afterTagAdded: function(evt, ui) {
              if (!ui.duringInitialization) {
                var tag = JSON.stringify(ui.tagLabel);
                var tagJ = JSON.parse(tag);
                
                var data = {
                  name : tagJ,
                  name_jp : tagJ,
                  taggable_type : 'App\\Recruitment',
                  taggable_id : '{{ $recruiment->id }}'
                };
                console.log("data = " + JSON.stringify(data));
                jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                jQuery.ajax({
                  data: data,
                  url: "{{ route('tag.add.do') }}",
                  dataType: 'json',
                  success: function (data) {
                    console.log('Tag data = '+ JSON.stringify(data)); 
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });

              }
          },
          afterTagRemoved: function(evt, ui) {
            var tag = JSON.stringify(ui.tagLabel);
                var tagJ = JSON.parse(tag);
                
                var data = {
                  name : tagJ,
                  name_jp : tagJ,
                  taggable_type : 'App\\Recruitment',
                  taggable_id : '{{ $recruiment->id }}'
                };
                console.log("data = " + JSON.stringify(data));
                jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                jQuery.ajax({
                  data: data,
                  url: "{{ route('tag.delete') }}",
                  dataType: 'json',
                  success: function (data) {
                    console.log('Tag data = '+ JSON.stringify(data)); 
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });
          },
        });
    });
</script>


    <script type="text/javascript">

    function theFunction ($id,$idA) {
      var $url = "{{URL::to('admin/delete-apply')}}/"+$id+"-"+$idA;

        $.confirm({
        title: 'Xác nhận',
        content: 'Bạn có muôn xóa?',
        buttons: {
            confirm: function () {
                window.location.href = $url;
            },
            cancel: function () {
                //$.alert('Canceled!');
            },           
        }
        });
            }

      jQuery().ready(function () {
        // init select date form
        $('.date').datepicker({ dateFormat: 'yy/mm/dd' });
      });

      $('select').selectize({
          sortField: 'text'
      });

      tinymce.init({
        selector: '#recruiment_description',
        menubar: false,
        height: 300,
        toolbar_mode: 'wrap',
        toolbar_location: 'bottom',
        plugins: "link image code",
          toolbar: 'undo redo | styleselect | forecolor | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
        images_upload_handler: function (blobInfo, success, failure) {
          var xhr, formData;
          xhr = new XMLHttpRequest();
          xhr.withCredentials = false;
          xhr.responseType = 'json';
          xhr.open('POST', uploadImg2Url);
          xhr.onload = function() {
            var imageName;

            if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
            }
            imageName = xhr.response;
            console.log("Image name = "+ imageName);
            //var url = "${imgUrl}/images/".concat(imageName);
            success(imageName);
          };
          
          var fileName = "";
          if( typeof(blobInfo.blob().name) !== undefined )
              fileName = blobInfo.blob().name;
          else
              fileName = blobInfo.filename();
          
          formData = new FormData();
          formData.append('file', blobInfo.blob(), fileName);
          xhr.send(formData);
          },
          
          
          force_br_newlines : true,
          force_p_newlines : false,
          forced_root_block : '',
          //image_prepend_url: jobSearchRoot + "/images/",
          relative_urls : false,
          //document_base_url: jobSearchRoot + "/images/",
          valid_children : '+body[style]'
      });

    </script>
    <script>
      $(document).ready(function(){
        $("#myBtn").click(function(){
          $("#myModal").modal("toggle");
        });
        $("#addProfile").click(function(){
          $("#ModalAddprofile").modal("toggle");
        });
      });
    </script>
@endsection