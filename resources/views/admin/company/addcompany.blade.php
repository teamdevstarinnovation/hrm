@extends('layouts.admin')
@section('content')

<h3 class="mt-4">Thêm mới Công ty</h3>

<div class="row">
    <div class="col-lg-6">

        @if(count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                    </ul>
                </div>
            </div>
        @endif
        
        <form role="form" method="POST" action="">
        {{ csrf_field() }}
        <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
            <div class="form-group">
                <label>Tên Công Ty:</label>
                <input type="text" class="form-control" require name="company_name"  value="{{ old('company_name') }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Thuộc ngành nghề:</label>
                <input type="text" class="form-control" require name="industry_type"  value="{{ old('industry_type') }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Lĩnh vực hoạt động:</label>
                <input type="text" class="form-control" require name="field_of_activity"  value="{{ old('field_of_activity') }}" autocomplete="off"/>
            </div>

            <div class="form-group">
                <label>Tên giám đốc:</label>
                <input type="text" class="form-control" require name="director_name"  value="{{ old('director_name') }}" autocomplete="off"/>
            </div>

            <button type="submit" class="btn btn-success" style="margin-top: 10px">Thêm mới công ty</button>

        </form>

    </div>

</div>
@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
@endsection        