@extends('layouts.admin')
@section('customcss')
  <style>
    .add-note {
      background-color: #314963;
      height: 100px;
      width: 100px;
      position: fixed;
      bottom: 20%;
      right: 10%;
      border-radius: 50%;
      font-size: 70px;
    }

    .border-note {
      border-radius: 25px;
      border: 2px solid rgb(150, 150, 150);
      padding: 20px;
    }
  </style>
@endsection
@section('content')
<h3 class="mt-4">Chỉnh sửa Công Ty</h3>
<!--- Tags block -->
<div> 
<ul id="myTags">
    <!-- Existing list items will be pre-added to the tags -->
    @foreach ($company->tags as $item)
            <li>{{ $item->name }}</li>          
           
    @endforeach
  </ul>
</div>
<!-- End tags block-->

<div class="row">
  <div class="col-lg-6">

    @if(count($errors))
      <div class="form-group">
        <div class="alert alert-danger">
          <ul>
              @foreach($errors->all() as $error)
                <li>{{$error}}</li>
              @endforeach
          </ul>
        </div>
      </div>
    @endif

    <form role="form" method="POST" action="{{ route('doEditCompany') }}">
      {{ csrf_field() }}
      <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
        <div class="form-group">
          <input type="hidden" name="company_id"  value="{{ $company->id }}" />
          <label>Tên Công Ty:</label>
          <input type="text" class="form-control" require name="company_name"  value="{{ $company->name }}" autocomplete="off"/>
        </div>
              
        <div class="form-group">
          <label>Thuộc ngành nghề:</label>
          <input type="text" class="form-control" require name="industry_type"  value="{{ $company->industry_type }}" autocomplete="off"/>
        </div>
              
        <div class="form-group">
          <label>Lĩnh vực hoạt động:</label>
          <input type="text" class="form-control" require name="field_of_activity"  value="{{ $company->field_of_activity }}" autocomplete="off"/>
        </div>

        <div class="form-group">
          <label>Tên giám đốc:</label>
          <input type="text" class="form-control" require name="director_name"  value="{{ $company->director_name }}" autocomplete="off"/>
        </div>

        <button type="submit" class="btn btn-success" style="margin-top: 10px">Sửa</button> 

    </form>

  </div>

  <div class="col-lg-6">
      <button type="button" class="btn btn-info btn-lg add-note" data-toggle="modal" data-target="#myModal"><i style="margin-bottom: 12px;" class="fa fa-plus" aria-hidden="true"></i></button>
  </div>

</div>

<div class="row form-group border-note">
    <p><h3 style="width: 100%">Danh sách ghi chú</h3></p>
    <br>
    @if(isset($note) && is_null($note))
      <p>Không có note nào</p>
    @else
      @foreach ($note as $item)
        <p style="width: 100%">{{ $item->created_at }} - {{ $item->name }} - {{ $item->content }}</p>
      @endforeach
    @endif
</div>
  
  <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <form role="form" method="POST" action="{{ route('addNote') }}">
          {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Ghi chú</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <textarea id="content" name = "content" style="width: 100%; resize: none;" rows="5" placeholder="Điền ghi chú ở đây"></textarea>
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <input type="hidden" name="noteType"  value="company" />
                <input type="hidden" name="destination_id"  value="{{ $company->id }}" />
              </div>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-default">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>

@endsection

@section('customjs')
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

  <script src="{{ asset('admincp/js/tag-it.js') }}" ></script>

  <script>
    $.noConflict();
    jQuery(document).ready(function(){
      var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
     
      jQuery("#myTags").tagit({
          availableTags: sampleTags,  
          removeConfirmation: true,
            allowDuplicates: false,
            allowSpaces: true,
          afterTagAdded: function(evt, ui) {
              if (!ui.duringInitialization) {
                var tag = JSON.stringify(ui.tagLabel);
                var tagJ = JSON.parse(tag);
                
                var data = {
                  name : tagJ,
                  name_jp : tagJ,
                  taggable_type : 'App\\Company',
                  taggable_id : '{{ $company->id }}'
                };
                console.log("data = " + JSON.stringify(data));
                jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                jQuery.ajax({
                  data: data,
                  url: "{{ route('tag.add.do') }}",
                  dataType: 'json',
                  success: function (data) {
                    console.log('Tag data = '+ JSON.stringify(data)); 
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });

              }
          },
          afterTagRemoved: function(evt, ui) {
            var tag = JSON.stringify(ui.tagLabel);
                var tagJ = JSON.parse(tag);
                
                var data = {
                  name : tagJ,
                  name_jp : tagJ,
                  taggable_type : 'App\\Company',
                  taggable_id : '{{ $company->id }}'
                };
                console.log("data = " + JSON.stringify(data));
                jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                jQuery.ajax({
                  data: data,
                  url: "{{ route('tag.delete') }}",
                  dataType: 'json',
                  success: function (data) {
                    console.log('Tag data = '+ JSON.stringify(data)); 
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });
          },
        });
    });
</script>

  <script>
  $(document).ready(function(){
    $("#myBtn").click(function(){
      $("#myModal").modal("toggle");
    });
  });
  </script>
@endsection