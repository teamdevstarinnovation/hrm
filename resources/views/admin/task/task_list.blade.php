@extends('layouts.admin')
<style>
.action-button {
    width: 100px;
    background: skyblue;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px;
}
</style>
@section('content')

<h3 class="mt-4">Danh sách nhiệm vụ hôm nay</h3>
<div class="card mb-4 bg-warning">
    <div class="card-body">
        <ul>
            <li>Phía nguồn X vừa chuyển qua 4 bạn</li>
            <li>Cần thông báo phía nguồn về chính sách mới của công ty</li>
        </ul>
    </div>
</div>
<div class="card mb-4">
    
    <div class="card-body">
        <input type="button" style="margin-bottom: 15px;" onclick="location.href='{{ route('task.create') }}'" name="reigster" class="action-button" value="Thêm mới" />
        <div class="table-responsive">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">

                <div class="row">
                    <div class="col-sm-12">
                <table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 88.4px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Tên nhiệm vụ</th>
                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 143.967px;" aria-label="Position: activate to sort column ascending">Nội dung nhiệm vụ</th>
                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 67.3833px;" aria-label="Office: activate to sort column ascending">Deadline</th>
                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 67.3833px;" aria-label="Office: activate to sort column ascending"></th>
                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 67.3833px;" aria-label="Office: activate to sort column ascending"></th>
                </thead>

                <tbody>
                    @foreach ($tasks as $t)
                        <tr role="row" class="odd">
                        <td class="sorting_1">{{ $t->nameTask }}</td>
                            <td>{{ $t->contentTask }}</td>
                            <td>{{ $t->deadline }}</td>
                            <td><a href="{{url('admin/edit-resource', ['id' => $t->id])}}">Sửa</a></td>
                            <td><a href="#" onclick="return openPopup({{ $t->id }});">Xóa</a></td>
                        </tr>
                    @endforeach
                   </tbody>
            </table></div></div>
        </div>
        </div>
    </div>
</div>
<script>

function openPopup(id){
    $.confirm({
        title: 'Xác nhận',
        content: 'Bạn có muôn xóa?',
        buttons: {
            confirm: function () {
                window.location.href = 'delete-resource/'+id;
            },
            cancel: function () {
                //$.alert('Canceled!');
            },           
        }
    });
}
$(document).ready(function(){
       
});
</script>
@endsection

@section('customjs')   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admincp/assets/demo/datatables-demo.js') }}"></script>
    
@endsection