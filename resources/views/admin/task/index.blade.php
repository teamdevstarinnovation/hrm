@extends('layouts.admin')

@section('content')

<h3 class="mt-4">Nhiệm vụ ngày hôm nay</h3>
<div class="card mb-4 bg-warning">
    <div class="card-body">
        <ul>
            <li>Phía nguồn X vừa chuyển qua 4 bạn</li>
            <li>Cần thông báo phía nguồn về chính sách mới của công ty</li>
        </ul>
    </div>
</div>

@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admincp/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admincp/assets/demo/datatables-demo.js') }}"></script>
@endsection