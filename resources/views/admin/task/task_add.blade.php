@extends('layouts.admin')
@section('content')
<h3 class="mt-4">Thêm mới nhiệm vụ</h3>

<div class="row">
    <div class="col-lg-6">

      @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
      @endif

    <form role="form" method="POST" action="{{ route('task.create') }}">
      {{ csrf_field() }}
        <div class="form-group">
          <label style="margin-bottom: 15px; margin-top:15px">Tên nhiệm vụ:</label>
          <input type="text" class="form-control" name="task_name" value="" />
        </div>

        <div class="form-group">
          <label style="margin-bottom: 15px; margin-top:15px">Loại nhiệm vụ:</label>
          <select class="form-control" id="type_task" name="type_task">
            <option value="0">Cho Công ty</option>
            <option value="1">Cho ứng viên</option> 
          </select>
        </div>

        <div class="form-group">
            <label style="margin-bottom: 15px; margin-top:15px">Người nhận nhiệm vụ:</label>
            <select class="form-control" id="destination_task" name="destination_task">
            <option value="0">Linh</option>           
          </select>
        </div>

        <div class="form-group">
          <label style="margin-bottom: 15px; margin-top:15px">Ngày hoàn thành:</label>
          <input type="text" class="form-control date" name="deadline" placeholder="Deadline" value="" />
        </div>

        <div class="form-group" style="margin-bottom: 15px;">
          <label style="margin-bottom: 15px; margin-top:15px; display: list-item;">Trạng thái:</label>
          <label>
            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="1" checked=""> Đã hoàn thành
            </label>
            <label>
            <input style="margin-left:30px" type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="2"> Chưa hoàn thành
            </label>
        </div>

        <button type="submit" style="background-color: aqua;" class="btn btn-default">Đăng nhiệm vụ</button>

      </form>

    </div>
  </div>
@endsection

@section('customjs')
   
@endsection