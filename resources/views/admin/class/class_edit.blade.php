@extends('layouts.admin')
@section('customcss')
  <style>
    .add-note {
      background-color: #314963;
      height: 100px;
      width: 100px;
      position: fixed;
      bottom: 20%;
      right: 10%;
      border-radius: 50%;
      font-size: 70px;
    }

    .border-note {
      border-radius: 25px;
      border: 2px solid rgb(150, 150, 150);
      padding: 20px;
    }
  </style>

    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
@endsection
@section('content')

<h3 class="mt-4">Chỉnh sửa Lớp học</h3>
<div class="row">
    <div class="col-lg-6">

        @if(count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        
        <form role="form" method="POST" action="{{ route('doEditClass') }}">
        {{ csrf_field() }}
        <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
            <div class="form-group">
                <input type="hidden" name="class_id"  value="{{ $classes->id }}" />
                <label>Tên Lớp học:</label>
                <input type="text" class="form-control" require name="class_name"  value="{{ $classes->name }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Teacher:</label>
                <select id="teacher_id" name="teacher_id" placeholder="Chọn 1 giáo viên ...">
                    @foreach ($users as $item)
                        @if($item->id == $classes->teacher_id)
                          <option value="{{ $item->id }}" selected> {{ $item->name }} </option>
                        @else
                          <option value="{{ $item->id }}"> {{ $item->name }} </option>
                        @endif
                    @endforeach     
                </select>
            </div>

            <button type="submit" class="btn btn-success" style="margin-top: 10px">Chỉnh sửa lớp học</button>

        </form>
    </div>
    
    <div class="col-lg-6">
      <h5 style="text-align: center;">Tổng số học sinh tham gia lớp học ({{ count($profile_classes) }}) </h5>
      <hr />
      <ul style="list-style-type: none;">
        @foreach ($profile_classes as $item)
            <li><a href="{{ route('profile.detail', $item->profile_id) }}"> {{ $item->full_name }} </a> được thêm vào {{ $item->name }} | {{ $item->created_at }} 
            <a href="#" onclick="return theFunction({{$item->profile_id }},{{ $item->class_id}});"><i  class="fa fa-times" aria-hidden="true"></i></a></li>
        @endforeach
        
      </ul>

      <button type="button" id="addProfile" class="btn btn-success" style="">Thêm học sinh</button>

    </div>

    <div class="col-lg-6">
        <button type="button" class="btn btn-info btn-lg add-note" data-toggle="modal" data-target="#myModal"><i style="margin-bottom: 12px;" class="fa fa-plus" aria-hidden="true"></i></button>
    </div>

</div>

<div class="row form-group border-note">
    <p><h3 style="width: 100%">Danh sách ghi chú</h3></p>
    <br>
    @if(isset($note) && is_null($note))
      <p>Không có note nào</p>
    @else
      @foreach ($note as $item)
        <p style="width: 100%">{{ $item->created_at }} - {{ $item->name }} - {{ $item->content }}</p>
      @endforeach
    @endif
</div>

<!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <form role="form" method="POST" action="{{ route('addNote') }}">
          {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Ghi chú</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <textarea id="content" name = "content" style="width: 100%; resize: none;" rows="5" placeholder="Điền ghi chú ở đây"></textarea>
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <input type="hidden" name="noteType"  value="classes" />
                <input type="hidden" name="destination_id"  value="{{ $classes->id }}" />
              </div>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-default">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- Modal -->
       <div class="modal fade" id="ModalAddprofile" role="dialog">
        <div class="modal-dialog">
          <form role="form"  method="POST" action="{{ route('class.doAddProfile') }}">
          {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Danh sách ứng viên</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                      @foreach($profiles as $p)
                            <tr>
                              <td>
                              <input type="checkbox" name="checked[]" id="myCheck{{ $p->id }}" value="{{ $p->id }}">
                              </td>
                              <td><label for="myCheck{{ $p->id }}">{{$p->full_name }}</label> </td>
                            </tr>
                            <br>
                            @endforeach
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <input type="hidden" name="noteType"  value="classes" />
                <input type="hidden" name="destination_id"  value="{{ $classes->id }}" />
              </div>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-default">Thêm</button>
              </div>
            </div>
          </form>
        </div>
      </div>

@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>

    <script type="text/javascript">

      function theFunction ($id,$idA) {
      var $url = "{{URL::to('admin/delete-apply-class')}}/"+$id+"-"+$idA;

        $.confirm({
        title: 'Xác nhận',
        content: 'Bạn có muôn xóa?',
        buttons: {
            confirm: function () {
                window.location.href = $url;
            },
            cancel: function () {
                //$.alert('Canceled!');
            },           
        }
        });
            }

      $('select').selectize({
          sortField: 'text'
      });
      
    </script>
    <script>
      $(document).ready(function(){
        $("#myBtn").click(function(){
          $("#myModal").modal("toggle");
        });
        $("#addProfile").click(function(){
          $("#ModalAddprofile").modal("toggle");
        });
      });
    </script>
@endsection        