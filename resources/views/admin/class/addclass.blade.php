@extends('layouts.admin')
@section('customcss')
    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
@endsection
@section('content')

<h3 class="mt-4">Thêm mới Lớp học</h3>

<div class="row">
    <div class="col-lg-6">

        @if(count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        
        <form role="form" method="POST" action="">
        {{ csrf_field() }}
        <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
            <div class="form-group">
                <label>Tên Lớp học:</label>
                <input type="text" class="form-control" require name="class_name"  value="" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Teacher:</label>
                <select id="teacher_id" name="teacher_id" placeholder="Chọn 1 giáo viên ...">
                    @foreach ($users as $item)
                        <option value="{{ $item->id }}"> {{ $item->name }} </option>
                    @endforeach     
                </select>
            </div>

            <button type="submit" class="btn btn-success" style="margin-top: 10px">Thêm mới lớp học</button>

        </form>

    </div>

</div>
@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>

    <script type="text/javascript">
    
      $('select').selectize({
          sortField: 'text'
      });
      
    </script>
@endsection        