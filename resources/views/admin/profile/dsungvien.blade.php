@extends('layouts.admin')
<style>
.action-button {
    width: 100px;
    background: skyblue;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px;
}

 
   
</style>
@section('content')

<h3 class="mt-4">Danh Sách Ứng Viên - Cập nhật NewFeed</h3>
<div class="card mb-4 bg-warning">
    <div class="card-body">
        <ul> @foreach ($log as $l)
            <li>{{ $l->content }}</li>
            @endforeach  
        </ul>
    </div>
</div>
<div class="card mb-4">
    <div class="card-body">
    <input type="button" style="margin-bottom: 15px;" onclick="location.href='{{ route('profile.create') }}'" name="reigster" class="action-button" value="Đăng Ký" />
        <div class="table-responsive">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Họ tên</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Ngày sinh</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Quê quán</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Địa chỉ</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Giới tính</th>
                                    <!-- <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Số điện thoại</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Email</th> -->
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($profiles as $p)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1"><a href="{{ route('profile.detail', $p->id) }}"> {{ $p->full_name }} </a></td>
                                        <td>{{ \Carbon\Carbon::parse($p->birth)->format('d-m-Y')}}</td>
                                        <td>{{ $p->homeTown->name }}</td>
                                        <td>{{ $p->address_Ward->name }} - {{ $p->address_District->name }} - {{ $p->address_City->name }}</td>
                                        <td>
                                            @if($p->gender == 0)
                                                Nữ
                                            @else
                                                Nam
                                            @endif
                                        </td>
                                        <!-- <td>{{ $p->phone_number }}</td>
                                        <td>{{ $p->email }}</td> -->
                                        <td> @if($p->status_id == 1)
                                            Chưa phỏng vấn
                                            @elseif($p->status_id == 2)
                                            Đã đậu phỏng vấn
                                            @elseif($p->status_id == 3)
                                            Đã trượt phỏng vấn
                                            @elseif($p->status_id == 4)
                                            Đã có tư cách lưu trú
                                            @elseif($p->status_id == 5)
                                            Trượt tư cách lưu trú
                                            @elseif($p->status_id == 6)
                                            Kết thúc
                                            @elseif($p->status_id == 7)                                   
                                            Không thể nhận lại hồ sơ
                                            @else
                                            Nộp hồ sơ lần 2
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admincp/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
 $( document ).ready(function() {
  $('#dataTable').DataTable({
    "pagingType": "full_numbers",
    "language": {
      "search": "Tìm kiếm: ",
      "lengthMenu": "Hiển thị _MENU_ kết quả",
      "paginate" : {
        "first":    'Đầu',
        "previous": 'Trước',
        "next":     'Tiếp',
        "last":     'Cuối'
      }
    },
    "scrollY":        "300px",
    "scrollX":        true,
    "scrollCollapse": true,
    "paging":         true,
    "columnDefs": [
            { width: '15%', targets: 0 },
            { width: '17%', targets: 1 },
            { width: '18%', targets: 2 },
            { width: '18%', targets: 3 },
            { width: '16%', targets: 4 },
            { width: '18%', targets: 5 },
        ],
        "fixedColumns": true,
    "info" : false
  });
});
    </script>
@endsection