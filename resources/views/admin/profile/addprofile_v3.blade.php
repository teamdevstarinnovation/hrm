@extends('layouts.admin')
@section('customcss')
@endsection
@section('content')

    <h1 class="mt-4">Thêm mới ứng viên</h1>

    @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <form action="forms/ticket/" method="post" id="ticketForm">
    <div class="container sub-page ticketForm">
        	<h2>Ticket Support</h2>

        <p>We understand you need help with our products at times, by completing the steps below you are able to provide us with all the information we need to get you on your way faster than ever before. Once you complete your ticket, you will receive a response from one of our support techs in 30 minutes or less.</p>
        <hr/>
        <div class="card-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card bg-success">
                <div class="card-heading" role="tab" id="generalHeading">
                    	<h4 class="card-title">
	        			General
	      			</h4>

                </div>
                <div id="generalInfo" class="collapse in" role="tabpanel">
                    <div class="card-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-xs-2">Name</label>
                                <div class="col-xs-10">
                                    <input type="text" name="name" id="name" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-2">E-mail</label>
                                <div class="col-xs-10">
                                    <input type="text" name="email" id="email" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-2">Phone Number</label>
                                <div class="col-xs-10">
                                    <input type="text" name="phone" id="phone" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-2">Customer Number</label>
                                <div class="col-xs-10">
                                    <input type="text" name="cNumber" id="cNumber" class="form-control" />	<span class="help-block">12 digit number on your customer card received by The Green Panda.</span>

                                </div>
                            </div>
                        </div>	<a data-parent="#accordion" href="#problemInfo" class="btn btn-success pull-right continue">Continue</a>

                    </div>
                </div>
            </div>
            <div class="card bg-success">
                <div class="card-heading" role="tab" id="problemHeading">
                    	<h4 class="card-title">
	        			Issue
	      			</h4>

                </div>
                <div id="problemInfo" class="collapse" role="tabpanel">
                    <div class="card-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-xs-2">Issue Service</label>
                                <div class="col-xs-10">
                                    <select name="issue_service" class="form-control">
                                        <option selected disabled>Select Your Service</option>
                                        <option value="editor-hosting">Website Editor Hosting</option>
                                        <option value="alone-hosting">Stand-Alone Web Hosting</option>
                                        <option value="editor-use">Website Editor Access/Use</option>
                                        <option value="general-issue">General Website Issue</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-2">Issue Description</label>
                                <div class="col-xs-10">
                                    <textarea class="form-control" name="issue_description"></textarea>	<span class="help-block">Please provide as much information as possible.</span>

                                </div>
                            </div>
                        </div>	<a data-parent="#accordion" href="#generalInfo" class="btn btn-danger">Previous</a>
	<a data-parent="#accordion" href="#priorityInfo" class="btn btn-success pull-right continue">Continue</a>

                    </div>
                </div>
            </div>
            <div class="card bg-success">
                <div class="card-heading" role="tab" id="priorityHeading">
                    	<h4 class="card-title">
	        			Priority
	      			</h4>

                </div>
                <div id="priorityInfo" class="collapse" role="tabpanel">
                    <div class="card-body">
                        <p>In most cases, our response time for tickets is 30 minutes. This may be extended due to tech availability and number of open tickets. If you would rather decrease your ticket wait time, you may purchase this ticket.</p>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6 text-center">
                                <button id="ticketPay" class="btn btn-success" style="margin-bottom:10px;">Pay $5.00 to decrease wait.</button>
                                <p class="text-left"><small>You will receive instant assistance based on tech availability. If your wait time surpasses 10 minutes, you will be refunded your purchase.</small>

                                </p>
                                <input type="text" name="token" data-name="stripeToken" style="display:none;" />
                            </div>
                            <div class="col-lg-6 text-center">
                                <button type="submit" id="SubmitForm" class="btn btn-success">Continue with original wait.</button>
                                <p class="text-left"><small>You will receive assistance within 30 minutes based on tech availability and number of currently open tickets.</small>

                                </p>
                            </div>
                        </div>
                        <hr/>
                    </div>
                </div>
            </div>
            <div class="card bg-success">
                <div class="card-heading" role="tab" id="priorityHeading">
                    	<h4 class="card-title">
	        			Success
	      			</h4>

                </div>
                <div id="ticketDone" class="collapse" role="tabpanel">
                    <div class="card-body">
                        <p>In most cases, our response time for tickets is 30 minutes. This may be extended due to tech availability and number of open tickets. If you would rather decrease your ticket wait time, you may purchase this ticket.</p>
                        <hr/>
                        <div class="row"></div>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>
        <p>Do you require instant assistance? You may use the information provided on our <a href="#contact">contact page</a> to contact emergency tech support technicians.</p>
    </div>
</form>

@endsection

@section('customjs')
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
<script>
    $('.continue').click(function(e){
        e.preventDefault();
        var sectionValid = true;
        var collapse = $(this).closest('.panel-collapse.collapse');
        $.each(collapse.find('input, select, textarea'), function(){
            if(!$(this).valid()){
                sectionValid = false;
            }
        });
        if(sectionValid){
            collapse.collapse('toggle');
            collapse.parents('.panel').next().find('.panel-collapse.collapse').collapse('toggle');
        }
    });

    $('a[href="#generalInfo"]').click(function(e){
        e.preventDefault();
        $('#problemInfo').collapse('toggle');
        $('#generalInfo').collapse('toggle');
    });


    $("#ticketForm").validate({
        errorClass: "error text-warning",
        validClass: "success text-success",
        highlight: function (element, errorClass) {
            //alert('em');
            //$(element).fadeOut(100,function () {
                //$(element).fadeIn(100);
        // });
        },
        rules: {
            name: "required",
            email: {
                required: true,
                email: true,
            },
            issue_service: "required",
            issue_description: "required"
        },
        submitHandler: function (form) {
            var a = $(form).serialize();
            alert(a);
        },
    });
</script>
@endsection