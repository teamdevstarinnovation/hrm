@extends('layouts.admin')
@section('customcss')
    <link href="{{ asset('admincp/css/formMultiStep.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
    <style>
    .add-note {
      background-color: #314963;
      height: 100px;
      width: 100px;
      position: fixed;
      bottom: 20%;
      right: 10%;
      border-radius: 50%;
      font-size: 70px;
      z-index: 999999;
    }

    .border-note {
      border-radius: 25px;
      border: 2px solid rgb(150, 150, 150);
      padding: 20px;
    }
  </style>
@endsection
@section('content')

    <h3 class="mt-4">Thông tin chi tiết ứng viên</h3>
    
    <!--- Tags block -->
    <div> 
        <span class="bg-info">Thái độ tốt</span>
        <span class="bg-danger">Đẹp trai</span>
        <span class="bg-warning">Ăn mặc đẹp</span>
        <button class="btn btn-primary">Thêm mới nhãn</button>
    </div>
    <!-- End tags block-->

    @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <form role="form" id="msform">
        @csrf @method('post')

        <!-- Xác nhận lại toàn bộ thông tin -->
        <fieldset>
            <div class="form-card">

                <div class="row">
                    <div class="col-md-6">
                        <h5 class="fs-title">Thông tin cơ bản</h5>
                        <hr />
                       <php echo($profile); ?php>
                        <p>Họ và tên: <span id="txt_full_name">{{ $profile->full_name }}</span></p>
                        <p>Ngày tháng năm sinh: <span id="txt_birthday">{{ $profile->birth }}</span></p>
                        <p>Quê quán: <span id="txt_address">{{ $profile->homeTown->name }}</span></p>
                        <p>Địa chỉ thường trú (Tỉnh/TP): <span id="txt_current_address_city">{{ $profile->address_City->name }}</span></p>
                        <p>Địa chỉ thường trú (Quận/Huyện): <span id="txt_current_address_district">{{ $profile->address_District->name }}</span></p>
                        <p>Địa chỉ thường trú (Phường/Xã): <span id="txt_current_address_ward">{{ $profile->address_Ward->name }}</span></p>
                        <p>Số điện thoại: <span id="txt_phone_number">{{ $profile->phone_number }}</span></p>
                        <p>Giới tính: 
                            <span id="txt_gender">
                                @if($profile->gender == 0) 
                                    Nữ 
                                @else 
                                    Nam 
                                @endif
                            </span>
                        </p>
                        <p>Email: <span id="txt_email">{{ $profile->email }}</span></p>
                        <p>Nguồn: <span id="txt_email">{{ $profile->resource->name }}</span></p>
                    </div>

                    <div class="col-md-6">
                        <h5 class="heading-title ">Thông tin về bố</h5>
                        <hr />

                        <p>Họ và tên: <span id="txt_father_full_name">{{ $profile->father_fullname }}</span></p>
                        <p>Năm sinh: <span id="txt_father_year_of_birth">{{ $father_birth }}</span></p>
                        <p>Nghề nghiệp: <span id="txt_father_career">{{ $profile->father_career }}</span></p>
                        <p>Số điện thoại: <span id="txt_father_phone">{{ $profile->father_phone }}</span></p>

                        <h5 class="heading-title ">Thông tin về mẹ</h5>
                        <hr />

                        <p>Họ và tên: <span id="txt_mother_full_name">{{ $profile->mother_fullname }}</span></p>
                        <p>Năm sinh: <span id="txt_mother_year_of_birth">{{ $mother_birth }}</span></p>
                        <p>Nghề nghiệp: <span id="txt_mother_career">{{ $profile->email }}</span></p>
                        <p>Số điện thoại: <span id="txt_mother_phone">{{ $profile->mother_phone }}</span></p>

                    </div>
                </div><!-- end .row -->
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="heading-title">Thông tin về anh chị em</h5>
                        <hr />
                        <p>Số anh trai: <span id="txt_siblings_brothers">{{ $profile->siblings_brothers }}</span></p>
                        <p>Số em trai: <span id="txt_siblings_younger_brothers">{{ $profile->siblings_younger_brothers }}</span></p>
                        <p>Số chị gái: <span id="txt_siblings_sisters">{{ $profile->siblings_sisters }}</span></p>
                        <p>Số em gái: <span id="txt_siblings_younger_sisters">{{ $profile->siblings_younger_sisters }}</span></p>

                    </div>
                    <div class="col-md-6">
                        <h5 class="heading-title">Bằng cấp, chửng chỉ - Kỹ năng</h5>
                        <hr />

                        <p>Tên trường tốt nghiệp: <span id="txt_degree_college_name">{{ $profile->degree_college_name }}</span></p>
                        <p>Trình độ/Cấp bậc: <span id="txt_degree_level">{{ $profile->degree_college_name }}</span></p>
                        <p>Chuyên ngành: <span id="txt_degree_specialized">{{ $profile->degree_specialized }}</span></p>
                        <p>Từ: <span id="txt_degree_from">{{ $degree_from }}</span> Đến : <span id="txt_degree_to">{{ $degree_to }}</span></p>
                        <p>Kỹ năng đặc biệt: <span id="txt_special_skill">{{ $profile->special_skill }}<span></p> 
                        <p>Chứng chỉ kỹ năng: <span id="txt_other_certificate">{{ $profile->other_certificate }}<span></p> 
                        
                    </div>
                </div>
            </div>
            <input type="button" name="back"  id="back" class="btn action-button" value="Trở về" />
            <input type="button" name="delete_profile" onclick="Delete({{ $profile->id }})" id="delete_profile" class="action-button-previous" value="Xóa" />
            <input type="button" id="edit_profile" onclick="toEditPage({{ $profile->id }})" name="edit_profile" class="action-button" value="Sửa" />
        </fieldset>

        <!-- Bước cuối cùng, submit thành công -->
        <fieldset>
            <div class="form-card">
                <h2 class="fs-title text-center">Xem lại thông tin đã sửa, đã thêm!</h2> <br><br>
                <div class="row justify-content-center">
                    <div class="col-auto"><img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="img-fluid"></div>
                </div>
                <br><br>
                <div class="row justify-content-center">
                    <div class="col-md-7 col-auto text-center">
                        <h5>You Have Successfully Signed Up</h5>
                    </div>
                </div>
            </div>
        </fieldset>

    </form>

    <div class="row">
        <div class="col-lg-6">
            <button type="button" class="btn btn-info btn-lg add-note" data-toggle="modal" data-target="#myModal"><i style="margin-bottom: 12px;" class="fa fa-plus" aria-hidden="true"></i></button>
        </div>
    </div>

    <div class="row form-group" style="">
        <h5 style="text-align: center;">Thống kê ứng viên: </h5>
        <br /><hr />
        <ul style="list-style-type: none;">
         <li>Đã tham gia 10 đơn hàng</li>
         <li>Đơn hàng cuối cùng là của công ty NTT Data</li>
        </ul>
    </div>

    <div class="row form-group border-note">
        <p><h3 style="width: 100%">Danh sách ghi chú</h3></p>
        <br>
        @if(isset($note) && is_null($note))
            <h4>Không có note nào</h4>
        @else
            @foreach ($note as $item)
                <p style="width: 100%">{{ $item->created_at }} - {{ $item->name }} - {{ $item->content }}</p>
            @endforeach
        @endif
    </div>

    <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <form role="form" method="POST" action="{{ route('addNote') }}">
          {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Ghi chú</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <textarea id="content" name = "content" style="width: 100%; resize: none;" rows="5" placeholder="Điền ghi chú ở đây"></textarea>
                <input type="hidden" name="user_id" id="user_id" value="{{ auth()->user()->id }}"/>
                <input type="hidden" name="noteType"  value="profile" />
                <input type="hidden" name="destination_id"  value="{{ $profile->id }}" />
              </div>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-default">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>

@endsection

@section('customjs')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript">
    function Delete($id){
        var $url = "{{URL::to('admin/delete-profile')}}/"+$id+"-"+$("#user_id").val();

            $.confirm({
            title: 'Xác nhận',
            content: 'Bạn có muôn xóa?',
            buttons: {
                confirm: function () {
                    window.location.href = $url;
                },
                cancel: function () {
                    //$.alert('Canceled!');
                },           
            }
        });
                
    }

    $( "#back" ).click(function() {
        window.history.back();
    });

    function toEditPage($id){
        var $url = "{{URL::to('admin/edit-profile')}}/"+$id;     
        window.location.href = $url;      
    }
      
    </script>
@endsection
