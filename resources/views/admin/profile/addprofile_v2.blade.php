@extends('layouts.admin')
@section('customcss')
    <link href="{{ asset('admincp/css/formMultiStep.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
@endsection
@section('content')

    <h3 class="mt-4">Thêm mới ứng viên</h3>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <form role="form" id="msform" method="POST" action="{{ route('profile.create') }}">
        @csrf @method('post')
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active" id="account"><strong>Thông tin cơ bản</strong></li>
            <li id="personal"><strong>Thông tin gia đình</strong></li>
            <li id="payment"><strong>Bằng cấp, chửng chỉ - Kỹ năng</strong></li>
            <li id="confirm"><strong>Xác nhận thông tin</strong></li>
            <li id="finish"><strong>Kết thúc</strong></li>
        </ul>

        <!-- Thông tin cơ bản của ứng viên -->
        <fieldset>
            <div class="form-card">
                <h2 class="fs-title">Thông tin cơ bản</h2>
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="full_name"  placeholder="Họ và tên"  value="{{ old('full_name') }}" />
                        
                        <input type="text" readonly data-date-format="dd-mm-yyyy"  class="form-control" name="birthday" id="birthday"  placeholder="Ngày tháng năm sinh" value="{{ old('birthday') }}" />
                        
                        <label style="color: #2c3e50;font-weight: bold;">Quê quán</label>
                        <select name="address" id="address" class="form-control" required="" style="margin-top: 15px; margin-bottom: 15px">
                            <option value="{{ old('address') }}">Tỉnh / Thành phố</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>

                        <label style="color: #2c3e50;font-weight: bold;">Địa chỉ thường trú</label>
                        <select name="current_address_city" id="current_address_city" class="form-control" required="" style="margin-top: 15px; ">
                            <option value="{{ old('current_address_city') }}">Tỉnh / Thành phố</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                        
                        <select name="current_address_district" id="current_address_district" class="form-control" required="" style="margin-top: 15px; ">
                            <option value="{{ old('current_address_district') }}">Quận / Huyện</option>
                        </select>
                        
                        <select name="current_address_ward" id="current_address_ward" class="form-control" required="" style="margin-top: 15px; ">
                            <option value="{{ old('current_address_ward') }}">Phường / Xã</option>
                        </select>
                        
                    </div>

                    <div class="col-md-6">
                        
                        <input type="text" class="form-control" name="phone_number" placeholder="Số điện thoại" value="{{ old('phone_number') }}" />
                        <input type="text" class="form-control" placeholder="Địa chỉ email" name="email" value="{{ old('email') }}" />

                        <label style="color: #2c3e50;font-weight: bold;">Giới tính: </label>
                        <select name="gender" id="gender" class="form-control" style="margin-bottom: 15px;"> 
                            <option value="0">Nữ</option>
                            <option value="1">Nam</option>
                        </select>

                        <label style="color: #2c3e50;font-weight: bold;">Nguồn: </label>
                        <select name="resource" id="resource" class="form-control" style="margin-top: 15px; margin-bottom: 30px;">
                            @foreach ($resources as $item)
                            <option value="{{ $item->id }}"> {{ $item->name }} </option>
                            @endforeach    
                        </select>

                        <label style="color: #2c3e50;font-weight: bold;">Trạng thái: </label>
                        <select name="status" id="status" class="form-control" style="margin-top: 15px;">
                            @foreach ($status as $item)
                            <option value="{{ $item->id }}"> {{ $item->name }} </option>
                            @endforeach    
                        </select>
                        
                        <!-- <select name="resource" id="resource" class="form-control" style="margin-top: 15px;"> 
                            <option value="0">Nữ</option>
                            <option value="1">Nam</option>
                        </select> -->
                    </div>
                   
                </div><!-- end .row -->

            </div>
            <input type="button" name="next" class="next action-button"
                   value="Next Step"/>
        </fieldset>

        <!-- Thông tin về gia đình của ứng viên-->
        <fieldset>
            <div class="form-card">
                <h2 class="fs-title">Thông tin gia đình</h2>
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="">Thông tin về bố</h5>
                        <input type="text" class="form-control" placeholder="Họ và tên Bố" name="father_full_name"  value="{{ old('father_full_name') }}" />
                        
                        <input data-date-format="yyyy" readonly type="text" class="form-control" placeholder="Năm sinh" name="father_year_of_birth_ui" id="father_year_of_birth_ui" value="{{ old('father_year_of_birth_ui') }}" />
                        <input value="" type="hidden" id="father_year_of_birth" name="father_year_of_birth" />
                        
                        <input type="text" class="form-control" placeholder="Nghề nghiệp" name="father_career"  value="{{ old('father_career') }}" />
                        <input type="text" class="form-control" placeholder="Số điện thoại" name="father_phone"  value="{{ old('father_phone') }}" />
                    </div>
                    <div class="col-md-6">
                        <h5 class="">Thông tin về mẹ</h5>

                        <input type="text" class="form-control" placeholder="Họ và tên mẹ" name="mother_full_name"  value="{{ old('mother_full_name') }}" />
                        
                        <input data-date-format="yyyy" readonly type="text" class="form-control" placeholder="Năm sinh" name="mother_year_of_birth_ui" id="mother_year_of_birth_ui"  value="{{ old('mother_year_of_birth_ui') }}" />
                        <input value="" type="hidden" id="mother_year_of_birth" name="mother_year_of_birth" />
                        
                        <input type="text" class="form-control" placeholder="Nghề nghiệp" name="mother_career"  value="{{ old('mother_career') }}" />
                        <input type="text" class="form-control" placeholder="Số điện thoại" name="mother_phone"  value="{{ old('mother_phone') }}" />

                    </div>
                </div>

                <h5 class="">Thông tin về anh chị em</h5>
                <div class="row">
                    <div class="col-md-6">
                        <label for="siblings_brothers">Số anh trai</label>
                        <input type="text" class="form-control" placeholder="Số anh trai" name="siblings_brothers"  value="0" />

                        <label for="siblings_younger_brothers">Số em trai</label>
                        <input type="text" class="form-control" placeholder="Số em trai" name="siblings_younger_brothers"  value="0" />
                    </div>
                    <div class="col-md-6">
                        <label for="siblings_sisters">Số chị gái</label>
                        <input type="text" class="form-control" placeholder="Số chị gái" name="siblings_sisters"  value="0" />

                        <label for="siblings_younger_sisters">Số em gái</label>
                        <input type="text" class="form-control" placeholder="Số em gái" name="siblings_younger_sisters"  value="0" />
                    </div>
                </div>

            </div>
            <input type="button" name="previous"
                   class="previous action-button-previous" value="Previous"/> <input
                type="button" name="next" class="next action-button"
                value="Next Step"/>
        </fieldset>

        <!-- Thông tin về bằng cấp, chứng chỉ của ứng viên -->
        <fieldset>
            <div class="form-card">
                <h2 class="fs-title">Bằng cấp, chửng chỉ - Kỹ năng</h2>
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" class="form-control" placeholder="Tên trường tốt nghiệp" name="degree_college_name"  value="{{ old('degree_college_name') }}" />
                        <input type="text" class="form-control" placeholder="Trình độ/Cấp bậc" name="degree_level"  value="{{ old('degree_level') }}" />
                        <input type="text" class="form-control" placeholder="Chuyên ngành" name="degree_specialized"  value="{{ old('degree_specialized') }}" />

                        <input data-date-format="mm-yyyy" readonly type="text" class="form-control" placeholder="Từ" name="degree_from_ui" id="degree_from_ui" value="{{ old('degree_from_ui') }}" />
                        <input value="" type="hidden" id="degree_from" name="degree_from" />

                        <input data-date-format="mm-yyyy" readonly type="text" class="form-control" placeholder="Đến" name="degree_to_ui" id="degree_to_ui" value="{{ old('degree_to_ui') }}" />
                        <input value="" type="hidden" id="degree_to" name="degree_to" />

                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" placeholder="Kỹ năng đặc biệt" name="special_skill"  value="{{ old('special_skill') }}" />
                        <input type="text" class="form-control" placeholder="Chứng chỉ khác" name="other_certificate"  value="{{ old('other_certificate') }}" />
                    </div>
                </div> <!-- end .row -->

            </div>

            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next Step" />
        </fieldset>


        <!-- Xác nhận lại toàn bộ thông tin -->
        <fieldset>
            <div class="form-card">

                <div class="row">
                    <div class="col-md-6">
                        <h5 class="fs-title">Thông tin cơ bản</h5>
                        <hr />

                        <p>Họ và tên: <span id="txt_full_name"></span></p>
                        <p>Ngày tháng năm sinh: <span id="txt_birthday"></span></p>
                        <p>Quê quán: <span id="txt_address"></span></p>
                        <p>Địa chỉ thường trú (Tỉnh/TP): <span id="txt_current_address_city"></span></p>
                        <p>Địa chỉ thường trú (Quận/Huyện): <span id="txt_current_address_district"></span></p>
                        <p>Địa chỉ thường trú (Phường/Xã): <span id="txt_current_address_ward"></span></p>
                        <p>Số điện thoại: <span id="txt_phone_number"></span></p>
                        <p>Giới tính: <span id="txt_gender"></span></p>
                        <p>Email: <span id="txt_email"></span></p>
                    </div>

                    <div class="col-md-6">
                        <h5 class="heading-title ">Thông tin về bố</h5>
                        <hr />

                        <p>Họ và tên: <span id="txt_father_full_name"></span></p>
                        <p>Năm sinh: <span id="txt_father_year_of_birth_ui"></span></p>
                        <p>Nghề nghiệp: <span id="txt_father_career"></span></p>
                        <p>Số điện thoại: <span id="txt_father_phone"></span></p>

                        <h5 class="heading-title ">Thông tin về mẹ</h5>
                        <hr />

                        <p>Họ và tên: <span id="txt_mother_full_name"></span></p>
                        <p>Năm sinh: <span id="txt_mother_year_of_birth_ui"></span></p>
                        <p>Nghề nghiệp: <span id="txt_mother_career"></span></p>
                        <p>Số điện thoại: <span id="txt_mother_phone"></span></p>

                    </div>
                </div><!-- end .row -->
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="heading-title">Thông tin về anh chị em</h5>
                        <hr />
                        <p>Số anh trai: <span id="txt_siblings_brothers">0</span></p>
                        <p>Số em trai: <span id="txt_siblings_younger_brothers">0</span></p>
                        <p>Số chị gái: <span id="txt_siblings_sisters">0</span></p>
                        <p>Số em gái: <span id="txt_siblings_younger_sisters">0</span></p>

                    </div>
                    <div class="col-md-6">
                        <h5 class="heading-title">Bằng cấp, chửng chỉ - Kỹ năng</h5>
                        <hr />

                        <p>Tên trường tốt nghiệp: <span id="txt_degree_college_name"></span></p>
                        <p>Trình độ/Cấp bậc: <span id="txt_degree_level"></span></p>
                        <p>Chuyên ngành: <span id="txt_degree_spe       cialized"></span></p>
                        <p>Từ: <span id="txt_degree_from_ui"></span> Đến : <span id="txt_degree_to_ui"></span></p>
                        <p>Kỹ năng đặc biệt: <span id="txt_special_skill"><span></p> 
                        <p>Chứng chỉ kỹ năng: <span id="txt_other_certificate"><span></p> 
                    </div>
                </div>
            </div>

            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
            <input type="button" id="add_profile" name="add_profile" class="next action-button" value="Thêm mới" />
        </fieldset>

        <!-- Bước cuối cùng, submit thành công -->
        <fieldset>
            <div class="form-card">
                <h2 class="fs-title text-center">Xem lại thông tin đã sửa, đã thêm!</h2> <br><br>
                <div class="row justify-content-center">
                    <div class="col-auto"><img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="img-fluid"></div>
                </div>
                <br><br>
                <div class="row justify-content-center">
                    <div class="col-md-7 col-auto text-center">
                        <h5>You Have Successfully Signed Up</h5>
                    </div>
                </div>
            </div>
        </fieldset>

    </form>
@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>

    <script type="text/javascript">
        jQuery().ready(function () {
            
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(document).on('change','#address', function(){
                var address = $('#address').val();
                console.log(address);
            });
            
            $(document).on('change','#current_address_ward', function(){
                var ward_id = $('#current_address_ward').val();
                console.log(ward_id);
            });

            $(document).on('change','#current_address_city',function(){
                var city_id = $(this).val();
                var div = $(this).parent();
                console.log(city_id);        
                var op = "";
                $.ajax({            
                    type: 'GET',
                    url: '{!!URL::to('admin/districts')!!}',
                    data: {'cityid' : city_id},
                    success:function(data){
                        op += '<option value="{{ old('current_address_district') }}">Quận / Huyện</option>';
                        for(var i=0; i<data.length; i++) {
                            op += '<option value="'+data[i].id+'">'+ data[i].name +'</option>'
                        }
                        div.find('#current_address_district').html(" ");
                        div.find('#current_address_district').append(op);
                        
                    },
                    error:function(data){
                        console.log("Error!")
                    }
                });
            });

            $(document).on('change','#current_address_district',function(){
                var district_id = $(this).val();
                var div = $(this).parent();
                console.log(district_id);
                var op = "";
                $.ajax({        
                    type: 'GET',
                    url: '{!!URL::to('admin/wards')!!}',
                    data: {'districtid' : district_id},
                    success:function(data){
                        op += '<option value="{{ old('current_address_ward') }}">Phường / Xã</option>';
                        for(var i=0; i<data.length; i++) {
                            op += '<option value="'+data[i].id+'">'+ data[i].name +'</option>'
                        }
                        div.find('#current_address_ward').html(" ");
                        div.find('#current_address_ward').append(op);
                    },
                    error:function(data){
                        console.log("Error!")
                    }
                });
            });

            $('#birthday').datepicker({
                weekStart: 1,
                daysOfWeekHighlighted: "6,0",
                autoclose: true,
                todayHighlight: true,
                locale: 'vi'
            });
        

            $( "#mother_year_of_birth_ui, #father_year_of_birth_ui" ).each(function(){
                $(this).datepicker({
                    viewMode: "years", 
                    minViewMode: "years",
                    autoclose: true,
                }).on('changeDate', function(e){
                    var selectDate = moment(e.date).format('DD-MM-YYYY');
                    console.log(selectDate);
                    var name = $(this).attr("name").replace(/_ui/, '');
                    console.log('name = #ß'+ name);
                    $("#" + name).val(selectDate);
                    $("#txt_" + name).text(e.date.getFullYear());
                });
            });

            
            $( "#degree_from_ui, #degree_to_ui" ).each(function(){
                $(this).datepicker({
                    viewMode: "months", 
                    minViewMode: "months",
                    autoclose: true,
                }).on('changeDate', function(e){    
                    var selectDate = moment(e.date).format('DD-MM-YYYY');
                    console.log(selectDate);
                    var name = $(this).attr("name").replace(/_ui/, '');
                    $("#" + name).val(selectDate);
                });
            });
            // --- Kết thúc xử lý ngày tháng

            // formMultiStep value when focus out
            $("#msform input[type=text]").each(function() {
                
                // Gán giá trị khi load page
                let inputValue = $(this).val();
                $("#txt_" + $(this).attr("name")).text(inputValue);


                $(this).on("change paste keyup", function() {
                    console.log(' Change value = ' + $(this).attr("name"));
                    let inputValue = $(this).val();
                    $("#txt_" + $(this).attr("name")).text(inputValue);
                });
            });

            $("#msform select").each(function() {
                
                // Gán giá trị khi load page
                
                if($(this).attr('id') === 'gender'){
                    let inputValue = $(this).val();
                    if(inputValue === '0'){
                        $("#txt_" + $(this).attr("name")).text('Nữ');
                    } else {
                        $("#txt_" + $(this).attr("name")).text('Nam');
                    }
                } else {
                    let inputValue = $(this).val();
                    $("#txt_" + $(this).attr("name")).text(inputValue);
                }
                
                $(this).on("change paste keyup", function() {
                    //console.log($(this).attr("name"));
                    if($(this).attr('id') === 'gender'){
                        let inputValue = $(this).val();
                        if(inputValue === '0'){
                            $("#txt_" + $(this).attr("name")).text('Nữ');
                        } else {
                            $("#txt_" + $(this).attr("name")).text('Nam');
                        }
                    } else {
                        let inputValue = $(this).val();
                        $("#txt_" + $(this).attr("name")).text(inputValue);
                    }
                });
            });

            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;

            $(".next").click(function () {
                
                var form = $("#msform");
                form.validate({
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element, errorClass, validClass) { 
                        $(element).closest('.form-group').addClass("has-error");
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group').removeClass("has-error");
                    },
                    rules: {
                        full_name: {
                            required: true,
                        },
                        birthday: {
                            required: true,
                        },
                        current_address: {
                            required: true,
                        },
                        phone_number: {
                            required: true,
                            number : true,
                        },
                        gender : {
                            required: true,
                        },
                        email : {
                            required: true,
                            email: true,
                        },
                        mother_phone:{
                            number : true,
                        },
                        father_phone :{
                            number : true,
                        }
                    },
                    messages: {
                        phone_number: {
                           
                            number : "Hãy nhập số ",
                        },
                        email : { 
                            
                            email: "nhập đúng định dạng email ",
                        },
                        mother_phone:{
                            number : "Hãy nhập số ",
                        },
                        father_phone :{
                            number : "Hãy nhập số ",
                        }
                       /* email: {
                            required: "email required",
                        },
                        uname: {
                            required: "uname required",
                        },
                        pwd: {
                            required: "Password required",
                        },
                        cpwd: {
                            required: "Password required",
                            equalTo: "Password don't match",
                        },*/
                    }

                });
                if (form.valid() === true) { // All field is valid
                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();
                    // log next fs and current fs
                    if($(this).attr("id") === "add_profile"){
                        $.fn.addNewProfile();
                        return false;
                    }

                    console.log("All fields is valid, to tot next section");

                    //Add Class Active
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function (now) {
                            // for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({'opacity': opacity});
                        },
                        duration: 600
                    });
                }

            });

            $.fn.addNewProfile = function() {
                console.log("Submit form to add new profile");
                var values = $("form").serializeFormJSON();

                console.log("values "+ JSON.stringify(values));


                $("#msform").submit();
                // if submit success return true
                // else return false
            };

            $.fn.serializeFormJSON = function () {

                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };

            $(".previous").click(function () {

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({'opacity': opacity});
                    },
                    duration: 600
                });
            });

            $('.radio-group .radio').click(function () {
                $(this).parent().find('.radio').removeClass('selected');
                $(this).addClass('selected');
            });
        });

    </script>
@endsection
