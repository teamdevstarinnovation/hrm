@extends('layouts.admin')
@section('customcss')
    <link href="{{ asset('admincp/css/formMultiStep.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
    <style>
        .help-block{
            position: absolute;
            margin-top: -40px;
            color : red;
        }
        h6 {
            text-align: left;
        }
    </style>
@endsection
@section('content')

    <h3 class="mt-4">Sửa thông tin ứng viên</h3>

    @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
        <!-- Xác nhận lại toàn bộ thông tin -->
    
        <form role="form" id="msform" method="POST" action="{{ route('doEditProfile') }}">
        @csrf @method('post')
            <div class="form-card">
                <input type="hidden" id="profile_id"  name="profile_id"  value="{{ $profile->id }}" />
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="fs-title">Thông tin cơ bản</h5>
                        <hr />                       
                        <h6>Họ và tên:</h6><input type="text" class="form-control" name="full_name"  placeholder="Họ và tên"  value="{{ $profile->full_name }}" />
                        <h6>Ngày tháng năm sinh:</h6> <input data-date-format="dd-mm-yyyy" type="text" class="form-control" id="birthday" name="birthday"  placeholder="Ngày tháng năm sinh" value="{{ $profile->birth }}" />
                        <h6>Quê quán: </h6>
                            <select name="address" id="address" class="form-control" required="" style="margin-top: 15px">
                                @foreach($cities as $city)
                                    @if ( $profile->homeTown->name == $city->name )
                                        <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                    @else
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        <h6 style="margin-top: 10px">Địa chỉ thường trú:</h6>
                            <select name="current_address_city" id="current_address_city" class="form-control" required="" >
                                @foreach($cities as $city)
                                    @if ( $profile->address_City->name == $city->name )
                                        <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                    @else
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            
                            <select name="current_address_district" id="current_address_district" class="form-control" required="" style="margin-top: 15px; ">
                                @foreach($districts as $district)
                                    @if ( $profile->address_District->name == $district->name )
                                        <option value="{{ $district->id }}" selected>{{ $district->name }}</option>
                                    @else
                                        <option value="{{ $district->id }}">{{ $district->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            
                            <select name="current_address_ward" id="current_address_ward" class="form-control" required="" style="margin-top: 15px; ">
                                @foreach($wards as $ward)
                                    @if ( $profile->address_Ward->name == $ward->name )
                                        <option value="{{ $ward->id }}" selected>{{ $ward->name }}</option>
                                    @else
                                        <option value="{{ $ward->id }}">{{ $ward->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        <h6>Giới tính: </h6>
                        <select name="gender" id="gender" class="form-control" style="margin-bottom: 15px;"> 
                        @if ($profile->gender === 0)
                        <option value="0" checked>Nữ</option>
                        <option value="1">Nam</option>
                        @else
                        <option value="1"checked>Nam</option>
                        <option value="0">Nữ</option>
                        @endif
                        </select>
                        <h6 style="margin-top: 15px">Số điện thoại:</h6> <input type="text" class="form-control" name="phone_number" placeholder="Số điện thoại" value="{{ $profile->phone_number }}" />
                        <h6>Email: </h6><input type="text" class="form-control" placeholder="Địa chỉ email" name="email" value="{{ $profile->email }}" />
                    </div>

                    <div class="col-md-6">
                    <h5 class="heading-title">Bằng cấp, chửng chỉ - Kỹ năng</h5>
                        <hr />
                        <h6>Tên trường tốt nghiệp: </h6><input type="text" class="form-control" placeholder="Tên trường tốt nghiệp" name="degree_college_name"  value="{{ $profile->degree_college_name }}" />
                        <h6>Trình độ/Cấp bậc: </h6><input type="text" class="form-control" placeholder="Trình độ/Cấp bậc" name="degree_level"  value="{{ $profile->degree_level }}" />
                        <h6 >Chuyên ngành: </h6><input style="margin-top: 15px" type="text" class="form-control" placeholder="Chuyên ngành" name="degree_specialized"  value="{{ $profile->degree_specialized }}" />
                        <h6>Từ:</h6>
                            <input data-date-format="mm-yyyy" type="text" class="form-control date-picker-year-month" placeholder="Từ" id="degree_from_ui" name="degree_from_ui"  value="{{ $degree_from }}" /> 
                            <input value="" type="hidden" id="degree_from" name="degree_from" />
                        <h6>Đến : </h6> 
                            <input data-date-format="mm-yyyy" type="text" class="form-control date-picker-year-month" placeholder="Đến" id="degree_to_ui" name="degree_to_ui"  value="{{ $degree_to }}" />       
                            <input value="" type="hidden" id="degree_to" name="degree_to" />

                        <h6>Kỹ năng đặc biệt: </h6><input type="text" class="form-control" placeholder="Kỹ năng đặc biệt" name="special_skill"  value="{{ $profile->special_skill }}" />
                        <h6 style="margin-top:15px">Chứng chỉ kỹ năng: </h6><input type="text" class="form-control" placeholder="Chứng chỉ khác" name="other_certificate"  value="{{ $profile->other_certificate }}" />
                    
                        
                    </div>
                </div><!-- end .row -->
                <div class="row" style="margin-top: 30px;">
                    <div class="col-md-6">
                    <h5 class="heading-title ">Thông tin về bố</h5>
                        <hr />      
                        <h6>Họ và tên: </h6><input type="text" class="form-control" placeholder="Họ và tên Bố" name="father_full_name"  value="{{ $profile->father_full_name }}" />
                        <h6>Năm sinh: </h6>
                            <input data-date-format="yyyy" type="text" class="form-control date-picker-year" placeholder="Năm sinh" id="father_year_of_birth_ui" name="father_year_of_birth_ui"  value="{{ $father_birth }}" />
                            <input value="" type="hidden" id="father_year_of_birth" name="father_year_of_birth" />

                        <h6>Nghề nghiệp: </h6><input type="text" class="form-control" placeholder="Nghề nghiệp" name="father_career"  value="{{ $profile->father_career }}" />
                        <h6>Số điện thoại: </h6><input type="text" class="form-control" placeholder="Số điện thoại" name="father_phone"  value="{{ $profile->father_phone }}" />
                        <h6 style="margin-top: 30px" class="heading-title">Thông tin về anh chị em</h6>
                        <hr /> 
                        <h6>Số anh trai: </h6><input type="text" class="form-control" placeholder="Số anh trai" name="siblings_brothers"  value="{{ $profile->siblings_brothers }}" />
                        <h6>Số em trai: </h6><input type="text" class="form-control" placeholder="Số em trai" name="siblings_younger_brothers"  value="{{ $profile->siblings_younger_brothers }}" />
                        <h6>Số chị gái: </h6><input type="text" class="form-control" placeholder="Số chị gái" name="siblings_sisters"  value="{{ $profile->siblings_sisters }}" />
                        <h6>Số em gái: </h6> <input type="text" class="form-control" placeholder="Số em gái" name="siblings_younger_sisters"  value="{{ $profile->siblings_younger_sisters }}" />
                        
                    </div>
                    <div class="col-md-6">
                    <h5 class="heading-title ">Thông tin về mẹ</h5>
                        <hr />
                        <h6>Họ và tên: </h6><input type="text" class="form-control" placeholder="Họ và tên mẹ" name="mother_full_name"  value="{{ $profile->mother_full_name }}" />
                        <h6>Năm sinh: </h6>
                            <input data-date-format="yyyy" type="text" class="form-control date-picker-year" placeholder="Năm sinh" id="mother_year_of_birth_ui" name="mother_year_of_birth_ui"  value="{{ $mother_birth }}" />
                            <input value="" type="hidden" id="mother_year_of_birth" name="mother_year_of_birth" />

                        <h6>Nghề nghiệp: </h6><input type="text" class="form-control" placeholder="Nghề nghiệp" name="mother_career"  value="{{ $profile->mother_career }}" />
                        <h6>Số điện thoại: </h6><input type="text" class="form-control" placeholder="Số điện thoại" name="mother_phone"  value="{{ $profile->mother_phone }}" />

                        <h5 style="margin-top: 30px" class="heading-title ">Nguồn</h5>
                        <hr />
                        <select name="resource" id="resource" class="form-control" style="margin-top: 15px;">
                        @foreach ($resources as $item)
                        <option value="{{ $item->id }}" {{ ( $item->id == $profile->resource->id) ? 'selected' : '' }}> {{ $item->name }} </option>
                        @endforeach    
                        </select>

                        <label style="color: #2c3e50;font-weight: bold;">Trạng thái: </label>
                        <select name="status" id="status" class="form-control" style="margin-top: 15px;">
                        @foreach ($status as $item)
                        <option value="{{ $item->id }}"> {{ $item->name }} </option>
                        @endforeach    
                        </select>
                        
                    </div>

                </div>
            </div>

            <input type="button" id="previous" name="previous" class="previous action-button-previous" value="Quay lại" />
            <input type="submit" id="add_profile" name="add_profile" class="next action-button" value="Cập nhật" />
            </form>

@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>

    <script type="text/javascript">
        jQuery().ready(function () {
            
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(document).on('change','#address', function(){
                var address = $('#address').val();
                console.log(address);
            });
            
            $(document).on('change','#current_address_ward', function(){
                var ward_id = $('#current_address_ward').val();
                console.log(ward_id);
            });

            $(document).on('change','#current_address_city',function(){
                var city_id = $(this).val();
                var div = $(this).parent();
                console.log(city_id);        
                var op = "";
                $.ajax({            
                    type: 'GET',
                    url: '{!!URL::to('admin/districts')!!}',
                    data: {'cityid' : city_id},
                    success:function(data){
                        op += '<option  {{ old('current_address_district') }}">Quận / Huyện</option>';
                        for(var i=0; i<data.length; i++) {
                            op += '<option value="'+data[i].id+'">'+ data[i].name +'</option>'
                        }
                        div.find('#current_address_district').html(" ");
                        div.find('#current_address_district').append(op);
                        
                    },
                    error:function(data){
                        console.log("Error!")
                    }
                });
            });

            $(document).on('change','#current_address_district',function(){
                var district_id = $(this).val();
                var div = $(this).parent();
                console.log(district_id);
                var op = "";
                $.ajax({        
                    type: 'GET',
                    url: '{!!URL::to('admin/wards')!!}',
                    data: {'districtid' : district_id},
                    success:function(data){
                        op += '<option value="{{ old('current_address_ward') }}">Phường / Xã</option>';
                        for(var i=0; i<data.length; i++) {
                            op += '<option value="'+data[i].id+'">'+ data[i].name +'</option>'
                        }
                        div.find('#current_address_ward').html(" ");
                        div.find('#current_address_ward').append(op);
                    },
                    error:function(data){
                        console.log("Error!")
                    }
                });     
            });

            $('#birthday').datepicker({
                weekStart: 1,
                daysOfWeekHighlighted: "6,0",
                autoclose: true,
                todayHighlight: true,
                locale: 'vi'
            });

            $( "#mother_year_of_birth_ui, #father_year_of_birth_ui" ).each(function(){
                $(this).datepicker({
                    viewMode: "years", 
                    minViewMode: "years",
                    autoclose: true,
                }).on('changeDate', function(e){
                    var selectDate = moment(e.date).format('DD-MM-YYYY');
                    console.log(selectDate);
                    var name = $(this).attr("name").replace(/_ui/, '');
                    console.log('name = #ß'+ name);
                    $("#" + name).val(selectDate);
                });
            });

            $( "#degree_from_ui, #degree_to_ui" ).each(function(){
                $(this).datepicker({
                    viewMode: "months", 
                    minViewMode: "months",
                    autoclose: true,
                }).on('changeDate', function(e){
                    var selectDate = moment(e.date).format('DD-MM-YYYY');
                    console.log(selectDate);
                    var name = $(this).attr("name").replace(/_ui/, '');
                    $("#" + name).val(selectDate);
                });
            });

            var form = $("#msform");
                form.validate({
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-control').addClass("has-error");
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-control').removeClass("has-error");
                    },
                    rules: {
                        full_name: {
                            required: true,
                        },
                        birthday: {
                            required: true,
                        },
                        current_address: {
                            required: true,
                        },
                        phone_number: {
                            required: true,
                            number : true,
                        },
                        gender : {
                            required: true,
                        },
                        email : {
                            required: true,
                            email: true,
                        },
                        mother_phone:{
                            number : true,
                        },
                        father_phone :{
                            number : true,
                        }
                    },
                    messages: {
                        phone_number: {
                           
                            number : "Hãy nhập số ",
                        },
                        email : { 
                            
                            email: "nhập đúng định dạng email ",
                        },
                        mother_phone:{
                            number : "Hãy nhập số ",
                        },
                        father_phone :{
                            number : "Hãy nhập số ",
                        }
                       
                    },
                    submitHandler: function(form) {
                        
                        //console.log("Submit form to add new profile");
                        var values = $("form").serializeFormJSON();
                        alert("Đã cập nhật, trở về trang danh sách");
                        //console.log("values "+ JSON.stringify(values));
                        //alert("values "+ JSON.stringify(values));
                        //alert("hihi");
                        form.submit();
                    }

                });
                $.fn.serializeFormJSON = function () {

                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
                };
                $( "#previous" ).click(function() {
                    window.history.back();
                });
        });
    </script>
@endsection
