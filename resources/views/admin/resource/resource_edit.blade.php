@extends('layouts.admin')
@section('customcss')
    <link href="{{ asset('admincp/css/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admincp/css/jquery-ui.theme.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <style>
    .add-note {
      background-color: #314963;
      height: 100px;
      width: 100px;
      position: fixed;
      bottom: 20%;
      right: 10%;
      border-radius: 50%;
      font-size: 70px;
    }

    .border-note {
      border-radius: 25px;
      border: 2px solid rgb(150, 150, 150);
      padding: 20px;
    }
  </style>
@endsection
@section('content')

<h3 class="mt-4">Chỉnh sửa nguồn</h3>
<!--- Tags block -->
<div> 
    <!-- <span class="bg-info">Hỗ trợ tốt</span>
    <span class="bg-danger">Chuyển tiền đúng hạn</span>
    <span class="bg-warning">Thái độ ứng viên rất tốt</span> -->
    <ul id="myTags">
    <!-- Existing list items will be pre-added to the tags -->
    @foreach ($resource->tags as $item)
            <li>{{ $item->name }}</li>          
           
    @endforeach
  </ul>
    <!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addTagModal">Thêm nhãn</button> -->
</div>
<!-- End tags block-->

<div class="row">
    

      @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
      @endif

  <div class="col-lg-6">
    <form role="form" method="POST" action="{{ route('doEditResource') }}">
      {{ csrf_field() }}
        <div class="form-group">
        <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
        <input type="hidden" name="resource_id"  value="{{ $resource->id }}" />
          <label>Tên nguồn:</label>
          <input type="text" class="form-control" name="resource_name"  value="{{ $resource->name }}" />
        </div>

        <div class="form-group">
          <label>Địa chỉ:</label>
          <select name="resource_address" id="resource_address" class="form-control" required="" style="">
            <option value="{{ old('resource_address') }}">Tỉnh / Thành phố</option>
            @foreach($cities as $city)
              @if ( $resource->address == $city->id )
                  <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
              @else
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
              @endif
            @endforeach
          </select>
        </div>

        <div class="form-group">
            <label>Số điện thoại:</label>
            <input type="text" class="form-control" name="resource_phone" value="{{ $resource->phone }}" />
        </div> 

        <button type="submit" class="btn btn-success" style="margin-top: 10px">Cập nhật nguồn</button>

      </form>


    </div>


    <div class="col-lg-6">
        <h5>Thống kê nguồn: {{ $resource->name }}</h5>
        <ul>
          <li>Đã gửi qua {{ $resource->profiles->count() }} ứng viên</li>
        </ul>
        <button type="button" class="btn btn-info btn-lg add-note" data-toggle="modal" data-target="#myModal"><i style="margin-bottom: 12px;" class="fa fa-plus" aria-hidden="true"></i></button>
    </div>

  </div>

  <div class="row form-group border-note">
      <p><h3 style="width: 100%">Danh sách ghi chú</h3></p>
      <br>
      @if(isset($note) && is_null($note))
        <p>Không có note nào</p>
      @else
        @foreach ($note as $item)
            <p style="width: 100%">{{ $item->created_at }} - {{ $item->name }} - {{ $item->content }}</p>
        @endforeach
      @endif
  </div>

  <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <form role="form" method="POST" action="{{ route('addNote') }}">
          {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Ghi chú</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <textarea id="content" name = "content" style="width: 100%; resize: none;" rows="5" placeholder="Điền ghi chú ở đây"></textarea>
                <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
                <input type="hidden" name="noteType"  value="resource" />
                <input type="hidden" name="destination_id"  value="{{ $resource->id }}" />
              </div>
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-default">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>

<!-- Modal -->
<div id="addTagModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thêm mới nhãn</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">Tag name</div>
                    <div class="col-md-7">
                        <input type="text" id="tag_name" name="tag_name" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">Tag name JP</div>
                    <div class="col-md-7">
                        <input type="text" id="tag_name_jp" name="tag_name_jp" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="modal-btn-add-tag">Thêm mới</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('customjs')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

<script src="{{ asset('admincp/js/tag-it.js') }}" ></script>

<script>
    $.noConflict();
    jQuery(document).ready(function(){
      var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
     
      jQuery("#myTags").tagit({
          availableTags: sampleTags,  
          removeConfirmation: true,
            allowDuplicates: false,
            allowSpaces: true,
          afterTagAdded: function(evt, ui) {
              if (!ui.duringInitialization) {
                var tag = JSON.stringify(ui.tagLabel);
                var tagJ = JSON.parse(tag);
                
                var data = {
                  name : tagJ,
                  name_jp : tagJ,
                  taggable_type : 'App\\Resource',
                  taggable_id : '{{ $resource->id }}'
                };
                console.log("data = " + JSON.stringify(data));
                jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                jQuery.ajax({
                  data: data,
                  url: "{{ route('tag.add.do') }}",
                  dataType: 'json',
                  success: function (data) {
                    console.log('Tag data = '+ JSON.stringify(data)); 
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });

              }
          },
          afterTagRemoved: function(evt, ui) {
            var tag = JSON.stringify(ui.tagLabel);
                var tagJ = JSON.parse(tag);
                
                var data = {
                  name : tagJ,
                  name_jp : tagJ,
                  taggable_type : 'App\\Resource',
                  taggable_id : '{{ $resource->id }}'
                };
                console.log("data = " + JSON.stringify(data));
                jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                jQuery.ajax({
                  data: data,
                  url: "{{ route('tag.delete') }}",
                  dataType: 'json',
                  success: function (data) {
                    console.log('Tag data = '+ JSON.stringify(data)); 
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
                });
          },
        });
    });
</script>
@endsection