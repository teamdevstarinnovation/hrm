@extends('layouts.admin')

@section('content')

<h3 class="mt-4">Thêm mới nguồn</h3>

<div class="row">
    <div class="col-lg-6">

      @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
      @endif

    <form role="form" method="POST" action="{{ route('doAddNewResource') }}">
      {{ csrf_field() }}
        <div class="form-group">
        <input type="hidden" name="user_id"  value="{{ auth()->user()->id }}"/>
          <label>Tên nguồn:</label>
          <input type="text" class="form-control" name="resource_name"  value="{{ old('resource_name') }}" />
        </div>

        <div class="form-group">
          <label>Địa chỉ:</label>
          <select name="resource_address" id="resource_address" class="form-control" required="" style="">
            <option value="{{ old('resource_address') }}">Tỉnh / Thành phố</option>
              @foreach($cities as $city)
                <option value="{{ $city->id }}">{{ $city->name }}</option>
              @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>Số điện thoại:</label>
            <input type="text" class="form-control" name="resource_phone" value="{{ old('resource_phone') }}" />
        </div>

        {{-- <div class="form-group">
          <label>Static Control</label>
          <p class="form-control-static">email@example.com</p>
        </div>

        <div class="form-group">
          <label>File input</label>
          <input type="file">
        </div>

        <div class="form-group">
          <label>Text area</label>
          <textarea class="form-control" rows="3"></textarea>
        </div>

        <div class="form-group">
          <label>Checkboxes</label>
          <div class="checkbox">
            <label>
              <input type="checkbox" value="">
              Checkbox  1
            </label>
          </div>
         <div class="checkbox">
            <label>
              <input type="checkbox" value="">
              Checkbox  2
            </label>
          </div>
         <div class="checkbox">
            <label>
              <input type="checkbox" value="">
              Checkbox  3
            </label>
          </div>
        </div>

        <div class="form-group">
          <label>Inline Checkboxes</label>
          <label class="checkbox-inline">
            <input type="checkbox"> 1
          </label>
          <label class="checkbox-inline">
            <input type="checkbox"> 2
          </label>
          <label class="checkbox-inline">
            <input type="checkbox"> 3
          </label>
        </div>

        <div class="form-group">
          <label>Radio Buttons</label>
          <div class="radio">
            <label>
              <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
              Radio  1
            </label>
          </div>
         <div class="radio">
            <label>
              <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
              Radio  2
            </label>
          </div>
         <div class="radio">
            <label>
              <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
              Radio  3
            </label>
          </div>
        </div>

        <div class="form-group">
          <label>Inline Radio Buttons</label>
          <label class="radio-inline">
            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="option1" checked=""> 1
          </label>
          <label class="radio-inline">
            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="option2"> 2
          </label>
          <label class="radio-inline">
            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline3" value="option3"> 3
          </label>
        </div>

        <div class="form-group">
          <label>Selects</label>
          <select class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
        </div>

        <div class="form-group">
          <label>Multiple Selects</label>
          <select multiple="" class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
        </div> --}}

        <button type="submit" class="btn btn-success" style="margin-top: 10px">Thêm mới nguồn</button> 
      </form>

    </div>
  </div>
@endsection

@section('customjs')
   
@endsection