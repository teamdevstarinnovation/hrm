@extends('layouts.admin')

@section('content')

<h1 class="mt-4">News feed</h1>
<div class="card mb-4 bg-warning">
    <div class="card-body">
        <ul>
            @foreach($system_logs as $sl)
                <li> {{ $sl->content }}</li>
            @endforeach
        </ul>
    </div>
</div>


<h1 class="mt-4">Statics - Hiển thị tất cả những thống kê ở đây đối với quyền giám đốc</h1>
<div class="card mb-4 bg-warning">
    <div class="card-body">
        <ul>
            <li>80% người đã đậu phỏng vấn khi uống nước mía</li>
            <li>200 ứng viên nói lắp không qua được vòng gửi xe</li>
        </ul>
    </div>
</div>

@endsection
