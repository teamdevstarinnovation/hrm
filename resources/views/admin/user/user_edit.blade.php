@extends('layouts.admin')
@section('content')

<h3 class="mt-4">Chỉnh sửa người dùng</h3>

<div class="row">
    <div class="col-lg-6">

        @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif

        <form role="form" method="POST" action="">
        {{ csrf_field() }}
        <input type="hidden" name="user_id2" id="user_id2"  value="{{ auth()->user()->id }}"/>
            <div class="form-group">
                <input type="hidden" name="user_id" value="{{ $user->id }}" />
                <div class="form-group">
                <label>Name:</label>
                <input type="text" class="form-control" name="user_name"  value="{{ $user->name }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Email:</label>
                <input type="text" disabled class="form-control" name="user_email"  value="{{ $user->email }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Password:</label>
                <input type="password" class="form-control" name="user_password"  value="" autocomplete="off"/>
            </div>

            <div class="form-group">
                <label for="role">Nhóm quyền:</label>
                <select id="role" name="role" placeholder="Chọn quyền...">
                    @foreach ($roles as $item)
                        @if($user->getRoleNames()->contains($item->name))
                            <option selected value="{{ $item->name }}"> {{ $item->name }} </option>
                        @else
                            <option value="{{ $item->name }}"> {{ $item->name }} </option>
                        @endif
                    @endforeach     
                </select>
            </div>

            <button type="submit" class="btn btn-success" style="margin-top: 10px">Cập nhật người dùng</button>
            </div>
        </form>

    </div>
</div>
@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
@endsection