@extends('layouts.admin')
@section('content')

<h3 class="mt-4">Thêm mới người dùng</h3>

<div class="row">
    <div class="col-lg-6">

        @if(count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        
        <form role="form" method="POST" action="">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" id="user_id"  value="{{ auth()->user()->id }}"/>
            <div class="form-group">
                <label>Name:</label>
                <input type="text" class="form-control" name="user_name"  value="{{ old('user_name') }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Email:</label>
                <input type="text" class="form-control" name="user_email"  value="{{ old('user_email') }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label>Password:</label>
                <input type="password" class="form-control" name="user_password"  value="{{ old('user_password') }}" autocomplete="off"/>
            </div>
            
            <div class="form-group">
                <label for="role"> Quyền:</label>
                <select id="role" name="role" placeholder="Chọn quyền...">
                    @foreach ($roles as $item)
                        <option value="{{ $item->name }}"> {{ $item->name }} </option>
                    @endforeach     
                </select>
            </div>

            <button type="submit" class="btn btn-success" style="margin-top: 10px">Thêm mới người dùng</button>

        </form>

    </div>
</div>
@endsection

@section('customjs')
    <script src="{{ asset('admincp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admincp/js/additional-methods.js') }}"></script>
    <script src="{{ asset('admincp/js/jquery-ui.min.js') }}"></script>
@endsection        