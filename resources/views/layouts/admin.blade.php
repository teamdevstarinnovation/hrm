

<!DOCTYPE html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>HRM - Star Innovations | Administrator</title>
        <link href="{{ asset('admincp/css/styles.css') }}" rel="stylesheet" />
        <link href="{{ asset('admincp/css/custom.css') }}" rel="stylesheet" />
        <link href="{{ asset('admincp/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />

        <link href="{{ asset('admincp/css/jquery.tagit.css') }}" rel="stylesheet" />
        <link href="{{ asset('admincp/css/tagit.ui-zendesk.css') }}" rel="stylesheet" />

        <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" />
        
        <link href="{{ asset('admincp/css/bootstrap-datepicker.min.css') }}" rel="stylesheet"/>

        <script src="{{ asset('admincp/js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('admincp/js/bootstrap.bundle.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/loadingoverlay.min.js') }}"></script>
        <script src="{{ asset('admincp/js/all.min.js') }}" ></script>
        <script src="{{ asset('admincp/js/bootstrap-datepicker.min.js') }}"></script>        
        <script src="{{ asset('admincp/js/moment.min.js') }}" ></script>
        
        @yield('customcss')
        
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ url('/admin') }}"><i class="fa fa-home" aria-hidden="true"></i>
 Home</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <!-- <h4 style="color : white;">Star Innovations HR Manager (Beta Test)</h4> -->
                    <!-- <i class="fa fa-list"></i> -->
                </div>
            </div>
            <!-- Navbar-->
            <div class="navbar-nav ml-auto ml-md-0 top-menu">
                <span style="color: white">Xin chào: {{ Auth::user()->name }}</span>
                <ul>
                    <li>
                        <a class="" href="{{ url('#') }}">
                            <i class="fa fa-bell" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a class="" href="{{ url('/logout') }}">                            
                            Đăng xuất
                        </a>
                    </li>
                </ul>
                
            </div>
            
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        @include('layouts.admin-left-menu')
                    </div>
                    <!-- <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div> -->
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Team IT 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="{{ asset('admincp/js/scripts.js') }}"></script>
        @yield('customjs')
    </body>
</html>
