<div class="nav">

    <!-- @if(auth()->user()->can('tasks.read'))
        <div class="sb-sidenav-menu-heading"><i class="fa fa-tasks"></i> Nhiệm vụ</div>
        <a class="nav-link" href="{{ route('task.list') }}"><div class="sb-nav-link-icon"><i class="fa fa-list"></i></div>Danh sách</a>
    @endif

    @if(auth()->user()->can('tasks.edit'))
        <a class="nav-link" href="{{ route('addTask') }}"><div class="sb-nav-link-icon"><i class="fa fa-plus-circle"></i></div>Thêm mới</a>
    @endif -->

    @if(auth()->user()->can('classes.read'))
        <div class="sb-sidenav-menu-heading"><i class="fa fa-tasks"></i> Quản lý lớp học</div>
        <a class="nav-link" href="{{ route('class.list') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-list"></i></div>Danh sách</a>
    @endif

    @if(auth()->user()->can('classes.edit'))
        <a class="nav-link" href="{{ route('addClass') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-plus-circle"></i></div>Thêm mới</a>
    @endif
    
    @if(auth()->user()->can('profiles.read'))
        <div class="sb-sidenav-menu-heading"><i class="fa fa-user-md"></i> Quản lý hồ sơ</div>
        <a class="nav-link" href="{{ route('profile.list') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-list"></i></div>Danh sách</a>
    @endif

    @if(auth()->user()->can('profiles.edit'))
        <a class="nav-link" href="{{ route('profile.create') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-plus-circle"></i></div>Thêm mới</a>
    @endif
    
    @if(auth()->user()->can('recruitments.read'))
        <div class="sb-sidenav-menu-heading"><i class="fa fa-address-card"></i> Quản lý đơn hàng</div>
        <a class="nav-link" href="{{ route('recruitment.list') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-list"></i></div>Danh sách</a>
    @endif
    @if(auth()->user()->can('recruitments.edit'))
        <a class="nav-link" href="{{ route('recruitment.create') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-plus-circle"></i></div>Thêm mới</a>
    @endif

    @if(auth()->user()->can('resources.read'))
        <div class="sb-sidenav-menu-heading"><i class="fa fa-list"></i> Quản lý nguồn</div>
        <a class="nav-link" href="{{ route('listResource') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-list"></i></div>Danh sách</a>
    @endif
    @if(auth()->user()->can('resources.edit'))
        <a class="nav-link" href="{{ route('addResource') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-plus-circle"></i></div>Thêm mới</a>
    @endif

    @if(auth()->user()->can('users.read'))
        <div class="sb-sidenav-menu-heading"><i class="fa fa-users"></i> Quản lý người dùng</div>
        <a class="nav-link" href="{{ route('listUser') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-list"></i></div>Danh sách</a>
    @endif

    @if(auth()->user()->can('users.edit'))
        <a class="nav-link" href="{{ route('addUser') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-plus-circle"></i></div>Thêm mới</a>
    @endif
    

    @if(auth()->user()->can('companies.read'))
        <div class="sb-sidenav-menu-heading"><i class="fa fa-list"></i> Quản lý Công Ty</div>
        <a class="nav-link" href="{{ route('company.list') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-list"></i></div>Danh sách</a> 
    @endif

    @if(auth()->user()->can('companies.edit'))
        <a class="nav-link" href="{{ route('addCompany') }}">&nbsp;&nbsp;&nbsp;&nbsp;<div class="sb-nav-link-icon"><i class="fa fa-plus-circle"></i></div>Thêm mới</a>
    @endif
  
            

</div>
