<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleHasPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $quyen_quantri = [
            'users.read',
            'users.edit',
            'users.delete',
            'recruitments.read',
            'recruitments.edit',
            'recruitments.delete',
            'resources.read',
            'resources.edit',
            'resources.delete',
            'profiles.read',
            'profiles.edit',
            'profiles.delete',
            'tasks.read',
            'tasks.edit',
            'tasks.delete',
            'companies.read',
            'companies.edit',
            'companies.delete',
            'classes.read',
            'classes.edit',
            'classes.delete',
            'tags.read',
            'tags.edit',
            'tags.delete',
        ];

        $role_quantri = Role::findByName('quantri');
        $role_quantri->givePermissionTo($quyen_quantri);

        $quyen_giamdoc = [
            'recruitments.read',
            'resources.read',
            'profiles.read',
            'companies.read',
            'classes.read',
            'tags.read',
            'tasks.read',
            'tasks.edit',
            'tasks.delete'
        ];
        $role_giamdoc = Role::findByName('giamdoc');
        $role_giamdoc->givePermissionTo($quyen_giamdoc);



        $quyen_truongphong = [
            'recruitments.read',
            'recruitments.edit',
            'recruitments.delete',
            'resources.read',
            'resources.edit',
            'resources.delete',
            'profiles.read',
            'profiles.edit',
            'profiles.delete',
            'tasks.read',
            'tasks.edit',
            'tasks.delete',
            'companies.read',
            'companies.edit',
            'companies.delete',
            'classes.read',
            'classes.edit',
            'classes.delete',
            'tags.read',
            'tags.edit',
            'tags.delete',
        ];

        $role_truongphong = Role::findByName('truongphong');
        $role_truongphong->givePermissionTo($quyen_truongphong);


        $quyen_thuky = [
            'recruitments.read',
            'recruitments.edit',
            'recruitments.delete',
            'resources.read',
            'resources.edit',
            'resources.delete',
            'profiles.read',
            'profiles.edit',
            'profiles.delete',
            'tasks.read',
            'tasks.edit',
            'companies.read',
            'companies.edit',
            'companies.delete',
            'classes.read',
            'classes.edit',
            'classes.delete',
            'tags.read',
            'tags.edit',
            'tags.delete',
        ];

        $role_thuky = Role::findByName('thuky');
        $role_thuky->givePermissionTo($quyen_thuky);
    }
}
