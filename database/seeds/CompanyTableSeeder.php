<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::truncate();

        $com = Company::create([
            'name'     => 'Stai Innovations',
            'industry_type'    => 'Human Resource',
            'field_of_activity' => 'Tuyển kỹ sư làm việc tại Nhật Bản',
            'director_name' => 'Uchida'
        ]);
    }
}
