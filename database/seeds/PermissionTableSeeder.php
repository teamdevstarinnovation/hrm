<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = [
            'users.read',
            'users.edit',
            'users.delete',
            'recruitments.read',
            'recruitments.edit',
            'recruitments.delete',
            'resources.read',
            'resources.edit',
            'resources.delete',
            'profiles.read',
            'profiles.edit',
            'profiles.delete',
            'tasks.read',
            'tasks.edit',
            'tasks.delete',
            'companies.read',
            'companies.edit',
            'companies.delete',
            'classes.read',
            'classes.edit',
            'classes.delete',
            'tags.read',
            'tags.edit',
            'tags.delete',
        ];
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
