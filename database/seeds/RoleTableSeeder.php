<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        
        $roles = [
            'quantri',
            'giamdoc',
            'truongphong',
            'thuky',
            'ketoan',
            'luatsu',
            'nhanvien',
            'hocvien',
            'congtacvien',
        ];
        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }
    }
}
