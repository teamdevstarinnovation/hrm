<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::truncate();
        // $r1 = new Status();
        // $r1->name = 'Chưa phỏng vấn';   
        // $r1->save();
        $status = [
            'Chưa phỏng vấn',
            'Đã đậu phỏng vấn',
            'Đã trượt phỏng vấn',
            'Đã có tư cách lưu trú',
            'Trượt tư cách lưu trú',
            'Kết thúc',
            'Không thể nhận lại hồ sơ',
            'Nộp hồ sơ lần 2'
        ];
        foreach ($status as $s) {
            Status::create(['name' => $s]);
        }
    }
}
