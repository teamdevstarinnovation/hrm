<?php

use App\User;
use Illuminate\Database\Seeder;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::Truncate();
        // admin
        $user = User::create([
            'name'     => 'Quản trị Viên',
            'email'    => 'admin@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('quantri');

        $user = User::create([
            'name'     => 'Giám đốc',
            'email'    => 'giamdoc@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('giamdoc');

        $user = User::create([
            'name'     => 'Trưởng Phòng',
            'email'    => 'truongphong@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('truongphong');

        $user = User::create([
            'name'     => 'Thư ký',
            'email'    => 'thuky@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('thuky');
    }
}
