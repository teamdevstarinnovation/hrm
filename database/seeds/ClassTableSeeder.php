<?php

use Illuminate\Database\Seeder;
use App\Classes;

class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = Classes::create([
            'name'     => 'Lớp tiếng Nhật N4',
            'teacher_id'    => 1,
        ]);
    }
}
