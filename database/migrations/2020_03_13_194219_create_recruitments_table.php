<?php

use App\Recruitment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Recruitment::truncate();

        Schema::create('recruitments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');

            $table->date('received_date');
            $table->date('interview_date');

            $table->string('visa_type');
            $table->string('position');

            $table->integer('require_numbers');
            $table->integer('numbers_of_interview');

            $table->string('work_place');
            $table->float('salary');

            $table->text('description');

            $table->bigInteger('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruitments');
    }
}
