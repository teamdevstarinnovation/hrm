<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nameTask');
            $table->string('contentTask');
            $table->bigInteger('userCreateTask_id');
            

            // taskType = 1 => Profile, taskType = 2 => Company, taskType = 3 => Résource, taskType = 4 => Recruitment 
            $table->integer('taskType');

            //  Profile id, company id, resource id, recruitment id.
            $table->bigInteger('destination_id');

            $table->timestamp('deadline');
            $table->integer('finish');
            $table->timestamps();
        });

        // Schema::table('tasks', function($table) {
        //     $table->foreign('userCreateTask_id')->references('id')->on('users');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
