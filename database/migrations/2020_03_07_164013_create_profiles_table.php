<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lastname')->nullable();
            $table->string('firstname')->nullable();
            $table->string('full_name')->nullable();
            $table->date('birth')->nullable();

            $table->unsignedBigInteger('address'); // Quê quán
            
            $table->unsignedBigInteger('current_address_city'); // địa chỉ hiện tại - tỉnh / thành phố
            
            $table->unsignedBigInteger('current_address_district'); // địa chỉ hiện tại - quận / huyện
            
            $table->unsignedBigInteger('current_address_ward'); // địa chỉ hiện tại - phường / xã  
            
            $table->string('phone_number', 20)->nullable();
            $table->string('gender')->nullable();
            $table->string('email')->unique();
           
            $table->string('father_fullname')->nullable(); // Họ và tên bố
            $table->string('father_phone')->nullable(); // Số điện thoại của bố
            $table->date('father_year_of_birth')->nullable(); // năm sinh của bố
            $table->string('father_career')->nullable(); // nghề nghiệp
            

            $table->string('mother_fullname')->nullable(); // Họ và tên mẹ
            $table->string('mother_phone')->nullable(); // số điện thoại
            $table->date('mother_year_of_birth')->nullable(); // năm sinh
            $table->string('mother_career')->nullable(); // nghề nghiệp


            $table->integer('siblings_brothers')->nullable(); //  Số anh trai
            $table->integer('siblings_younger_brothers')->nullable(); // số em trai
            $table->integer('siblings_sisters')->nullable(); // Số chị gái
            $table->integer('siblings_younger_sisters')->nullable(); // Số em gái

            $table->string('degree_college_name')->nullable(); // Tên trường đại học, cao đẳng
            $table->string('degree_level')->nullable(); //  Cấp bậc
            $table->string('degree_specialized')->nullable(); // chuyên ngành
            $table->date('degree_from')->nullable(); // Bắt đầu từ vd : 06/2019
            $table->date('degree_to')->nullable(); // Cho đến : 07/2021

            $table->date('submission_date')->nullable(); // Ngày nhận hồ sơ
            $table->date('submission_ID_date')->nullable(); // Ngày nộp cục, hồ sơ ở Việt Nam (Vietnam Immigration Department)
            $table->date('interview_date')->nullable(); // Ngày phỏng vấn

            $table->string('profile_status', 100)->nullable();
            // Chưa phỏng vấn, đã đậu phỏng vấn, đã trượt phỏng vấn
            // Đã có tư cách lưu trú hoặc trượt tư cách lưu trú
            // Kết thúc, không thể nhận lại hồ sơ nữa hoặc nộp lại hồ sơ lần 2
            
            $table->date('COE_date')->nullable(); // Ngày có tư cách lưu trú - Certificate of Eligibility
            $table->date('flight_date')->nullable(); // Ngày bay
            
            $table->string('special_skill')->nullable(); // Kỹ năng đặc biệt
            $table->string('other_certificate')->nullable(); // Chứng chỉ khác

            $table->unsignedBigInteger('resource_id');
            $table->foreign('resource_id')->references('id')->on('resources')->onDelete('cascade');

            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
