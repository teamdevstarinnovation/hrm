<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            
            $table->foreign('address')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('current_address_city')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('current_address_district')->references('id')->on('districts')->onDelete('cascade');
            $table->foreign('current_address_ward')->references('id')->on('wards')->onDelete('cascade');
            
        });
    }

}
