<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

// Facebook
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

// Error handle
Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);
Route::get('401',['as'=>'401','uses'=>'ErrorHandlerController@errorCode401']);
Route::get('403',['as'=>'403','uses'=>'ErrorHandlerController@errorCode403']);

Route::get('/', function(){
  return redirect()->route('login');
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['prefix'=>'admin', 'middleware' => ['role:quantri|giamdoc|truongphong|thuky']],function(){

    Route::get('/','AdminController@index')->name('home');
    
    Route::get('/list-class','ClassController@getClassList')->name('class.list');
    Route::get('/create-class','ClassController@getFormAddNewClass')->name('class.form');
    Route::post('/create-class','ClassController@doAddNewClass')->name('class.create');

    // Quản lý các nhiệm vụ của thành viên công ty
    Route::get('/list-task','TaskController@task')->name('task.list');
    Route::get('/create-task','TaskController@getFormAddNewTask')->name('addTask');
    Route::post('/create-task','TaskController@doAddNewTask')->name('task.create');
    Route::get('/delete-task/{id}','TaskController@deleteTask')->name('task.delete');

    // Profiles
    Route::get('/list-profile','ProfileController@profile')->name('profile.list');
    Route::get('/add-profile','ProfileController@getFormAddProfile')->name('addProfile');
    Route::get('/edit-profile/{id}','ProfileController@getFormEditProfile')->name('profile.edit');
    Route::post('/edit-profile','ProfileController@doEditProfile')->name('doEditProfile');
    Route::get('/detail-profile/{id}','ProfileController@getDetailProfile')->name('profile.detail');
    Route::post('/add-profile','ProfileController@doAddNewProfile')->name('profile.create');
    Route::get('/delete-profile/{id}-{idU}','ProfileController@doDeleteProfile')->name('profile.delete');
    Route::get('/districts','ProfileController@districts');
    Route::get('/wards','ProfileController@wards');

    // Recruitments
    Route::get('/list-recruitment','RecruitmentController@getRecruitmentList')->name('recruitment.list');
    Route::get('/add-recruitment','RecruitmentController@getFormAddRecruitment')->name('recruitment.create');
    Route::post('/add-recruitment','RecruitmentController@doAddNewRecruitment')->name('recruitment.store');
    Route::get('/edit-recruiment/{id}','RecruitmentController@getFormEditRecruiment')->name('recruiment.edit');
    Route::post('/edit-recruiment','RecruitmentController@doEditRecruiment')->name('recruitment.doEdit');
    Route::get('/delete-recruitment/{id}-{idU}','RecruitmentController@doDeleteRecruitment')->name('doDeleteRecruitment');
    Route::post('/recruitment-addprofile','RecruitmentController@doAddProfile')->name('recruitment.doAddProfile');
    Route::get('/delete-apply/{id}-{idA}','RecruitmentController@doDeleteApply')->name('doDeleteApply');
    

    // Resources
    Route::get('/list-resource','ResourceController@listResource')->name('listResource');
    Route::get('/add-resource','ResourceController@addResource')->name('addResource');
    Route::post('/add-resource','ResourceController@doAddNewResource')->name('doAddNewResource');
    Route::get('/edit-resource/{id}','ResourceController@editResource')->name('editResource');
    Route::post('/edit-resource','ResourceController@doEditResource')->name('doEditResource');
    Route::get('/delete-resource/{id}-{idU}','ResourceController@doDeleteResource')->name('doDeleteResource');

    // User
    Route::get('/list-users','UserController@listUser')->name('listUser');
    Route::get('/add-user','UserController@addUser')->name('addUser');
    Route::post('/add-user','UserController@doAddUser')->name('doAddUser');
    Route::get('/edit-user/{id}','UserController@editUser')->name('editUser');
    Route::post('/edit-user','UserController@doEditUser')->name('doEditUser');
    Route::get('/delete-user/{id}-{idU}','UserController@deDeleteUser')->name('doDeleteUser');

    // Notes
    Route::post('/add-note','NoteController@addNote')->name('addNote');

    // Company
    Route::get('/list-company','CompanyController@listCompany')->name('company.list');
    Route::get('/add-company','CompanyController@addCompany')->name('addCompany');
    Route::post('/add-company','CompanyController@doAddNewCompany')->name('doAddNewCompany');
    Route::get('/edit-company/{id}','CompanyController@editCompany')->name('editCompany');
    Route::post('/edit-company','CompanyController@doEditCompany')->name('doEditCompany');
    Route::get('/delete-company/{id}-{idU}','CompanyController@doDeleteCompany')->name('doDeleteCompany');

    //Class
    Route::get('/list-class','ClassController@listClass')->name('class.list');
    Route::get('/add-class','ClassController@addClass')->name('addClass');  
    Route::post('/add-class','ClassController@doAddClass')->name('doAddClass');
    Route::get('/edit-class/{id}','ClassController@editClass')->name('editClass');
    Route::post('/edit-class','ClassController@doEditClass')->name('doEditClass');
    Route::get('/delete-class/{id}-{idU}','ClassController@doDeleteClass')->name('doDeleteClass');
    Route::post('/class-addprofile','ClassController@doAddProfile')->name('class.doAddProfile');
    Route::get('/delete-apply-class/{id}-{idA}','ClassController@doDeleteApply')->name('doDeleteApplyClass');
    // System Logs
    
    // Facebook
    //Route::get('/fb/user', 'GraphController@retrieveUserProfile')->name('fb.user');

    // Tag manager
    Route::get('/add-tag','TagController@doAddTag')->name('tag.add.do');
    Route::get('/delete-tag','TagController@doDeleteTag')->name('tag.delete');
});


Route::group(['middleware' => ['auth']], function(){
  Route::get('/fb/user', 'GraphController@retrieveUserProfile')->name('fb.user');
});

Route::get('/superadmin', 'SuperAdminController@index');